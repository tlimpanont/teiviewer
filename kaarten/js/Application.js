(function(NA, jQuery, _, Backbone){
	
	jQuery(document).ready(function() {
		
		Utilities.optimizeTouchEvents();

		var teiviewer = new NA.MapTEIViewer({
			attributes: {
				mapId: "map"
			}
		});
	});

})(NA, jQuery, _, Backbone);	