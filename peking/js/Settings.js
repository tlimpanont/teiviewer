/**
 * @summary TEIviewer application that allows users to view and interact with ancient transcriptions and maps.
 * @file Manages the configuration settings and bootstrap TEIViewer application. 
 * @copyright Nationaal Archief 2013, GNU General Public License
 * @author Theuy Limpanont <theuy.limpanont@ordina.nl>
 */

/** 
 	@namespace
	@global
*/
window.NA = {};
NA.GPU_SUPPORT = false;
NA.data_folder = "data/";
NA.unselect_all_features_time = 120000;
NA.default_view_time = NA.unselect_all_features_time + 10000;
NA.fullscreen_detail_image_time = 120000;