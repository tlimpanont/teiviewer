(function(NA, jQuery, _, Backbone){
	
	jQuery(document).ready(function() {
		
		Utilities.optimizeTouchEvents();

		var teiviewer = new NA.PekingTEIViewer({
			attributes: {
				mapId: "map"
			}
		});
	});

})(NA, jQuery, _, Backbone);	