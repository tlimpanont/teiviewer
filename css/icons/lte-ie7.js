/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon-marker-flag' : '&#xe000;',
			'icon-button-popup-close' : '&#xe001;',
			'icon-button-plus' : '&#xe002;',
			'icon-button-minus' : '&#xe003;',
			'icon-button-info-trans' : '&#xe004;',
			'icon-button-info-trans-close' : '&#xe005;',
			'icon-button-info-map' : '&#xe006;',
			'icon-button-info-map-close' : '&#xe007;',
			'icon-button-close' : '&#xe008;',
			'icon-button-back-to-subgrid' : '&#xe009;',
			'icon-button-back-to-grid' : '&#xe00a;',
			'icon-arrow-overview-previous' : '&#xe00b;',
			'icon-arrow-overview-next' : '&#xe00c;',
			'icon-arrow-next' : '&#xe00d;',
			'icon-arrow-back' : '&#xe00e;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};