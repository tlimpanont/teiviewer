(function(NA, jQuery, _, Backbone){

	jQuery(document).ready(function() {
		
		Utilities.optimizeTouchEvents();
		
		var teiviewer = new NA.TranscriptionTEIViewer({
			attributes: {
				mapId: "map"
			}
		});
	});

})(NA, jQuery, _, Backbone);