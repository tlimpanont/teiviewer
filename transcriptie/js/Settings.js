/**
 * @summary TEIviewer application that allows users to view and interact with ancient transcriptions and maps.
 * @file Manages the configuration settings and bootstrap TEIViewer application. 
 * @copyright Nationaal Archief 2013, GNU General Public License
 * @author Theuy Limpanont <theuy.limpanont@ordina.nl>
 */

/** 
 	@namespace
	@global
*/
window.NA = {};
NA.GPU_SUPPORT = true;
NA.data_folder = "data/";
/* timeouts in millisecs */
NA.self_removing_time_for_read_label = 15000;
NA.default_view_time = NA.self_removing_time_for_read_label + 10000;
NA.documents_overview_time = NA.default_view_time + 10000;
/* end timeouts in millisecs */
