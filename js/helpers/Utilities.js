(function(window, jQuery) {
	
	/**
	 * Return all pattern matches with captured groups. It's like preg_match_all function of PHP.
	 * @param {String} string Input string to execute the matching.
	 * @returns {Array}
	 */
	RegExp.prototype.execAll = function(string) {
	    var match = null;
	    var matches = new Array();
	    while (match = this.exec(string)) {
	        var matchArray = [];
	        for (i in match) {
	            if (parseInt(i) == i) {
	                matchArray.push(match[i]);
	            }
	        }
	        matches.push(matchArray);
	    }
	    return matches;
	}

	/**
	 * This Utilities object DOES NOT include any third-party or open source library like jQuery. It is designed for implementing native features of javascript
	 * @class 
	 */
	window.Utilities = {};
	
	/*  
	 * Browser object represents the HTML browser. With this object we can the browser information in a cross browser platform way
	 */
	Utilities.Browser = {
		isTouch : function() {
			return (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));
		},
		getSize: function() {
			 var e = window, a = 'inner';
			if (!('innerWidth' in window )) {
				a = 'client';
				e = document.documentElement || document.body;
			}
			return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
		}
	}

	/*
	Browser name: BrowserDetect.browser
	Browser version: BrowserDetect.version
	OS name: BrowserDetect.OS
	*/
	Utilities.BrowserDetect  = {
		init: function () {
			this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
			this.version = this.searchVersion(navigator.userAgent)
				|| this.searchVersion(navigator.appVersion)
				|| "an unknown version";
			this.OS = this.searchString(this.dataOS) || "an unknown OS";
		},
		searchString: function (data) {
			for (var i=0;i<data.length;i++)	{
				var dataString = data[i].string;
				var dataProp = data[i].prop;
				this.versionSearchString = data[i].versionSearch || data[i].identity;
				if (dataString) {
					if (dataString.indexOf(data[i].subString) != -1)
						return data[i].identity;
				}
				else if (dataProp)
					return data[i].identity;
			}
		},
		searchVersion: function (dataString) {
			var index = dataString.indexOf(this.versionSearchString);
			if (index == -1) return;
			return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
		},
		dataBrowser: [
			{
				string: navigator.userAgent,
				subString: "Chrome",
				identity: "Chrome"
			},
			{ 	string: navigator.userAgent,
				subString: "OmniWeb",
				versionSearch: "OmniWeb/",
				identity: "OmniWeb"
			},
			{
				string: navigator.vendor,
				subString: "Apple",
				identity: "Safari",
				versionSearch: "Version"
			},
			{
				prop: window.opera,
				identity: "Opera",
				versionSearch: "Version"
			},
			{
				string: navigator.vendor,
				subString: "iCab",
				identity: "iCab"
			},
			{
				string: navigator.vendor,
				subString: "KDE",
				identity: "Konqueror"
			},
			{
				string: navigator.userAgent,
				subString: "Firefox",
				identity: "Firefox"
			},
			{
				string: navigator.vendor,
				subString: "Camino",
				identity: "Camino"
			},
			{		// for newer Netscapes (6+)
				string: navigator.userAgent,
				subString: "Netscape",
				identity: "Netscape"
			},
			{
				string: navigator.userAgent,
				subString: "MSIE",
				identity: "Explorer",
				versionSearch: "MSIE"
			},
			{
				string: navigator.userAgent,
				subString: "Gecko",
				identity: "Mozilla",
				versionSearch: "rv"
			},
			{ 		// for older Netscapes (4-)
				string: navigator.userAgent,
				subString: "Mozilla",
				identity: "Netscape",
				versionSearch: "Mozilla"
			}
		],
		dataOS : [
			{
				string: navigator.platform,
				subString: "Win",
				identity: "Windows"
			},
			{
				string: navigator.platform,
				subString: "Mac",
				identity: "Mac"
			},
			{
				   string: navigator.userAgent,
				   subString: "iPhone",
				   identity: "iPhone/iPod"
			},
			{
				string: navigator.platform,
				subString: "Linux",
				identity: "Linux"
			}
		]

	}; 	Utilities.BrowserDetect.init();

	
	/**
	 * Convert XML to JSON
	 * @param {XMLDoc} xml
	 * @returns {Object} JSON Object to work with
	 */
	Utilities.xmlToJSON = function(xml) {
		var xml = ((typeof xml) == "string") ? jQuery.parseXML(xml) : xml;
	
		// Changes XML to JSON, Source: http://davidwalsh.name/convert-xml-json
		function xmlToJSON(xml) {

			// Create the return object
			var obj = {};
			if (xml.nodeType == 1) { // element
				// do attributes
				if (xml.attributes.length > 0) {
				obj["@attributes"] = {};
					for (var j = 0; j < xml.attributes.length; j++) {
						var attribute = xml.attributes.item(j);
						obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
					}
				}
			} else if (xml.nodeType == 3) { // text
				obj = xml.nodeValue;
			}

			// do children
			if (xml.hasChildNodes()) {
				for(var i = 0; i < xml.childNodes.length; i++) {
					var item = xml.childNodes.item(i);
					var nodeName = item.nodeName;
					if (typeof(obj[nodeName]) == "undefined") {
						obj[nodeName] = xmlToJSON(item);
					} else {
						if (typeof(obj[nodeName].push) == "undefined") {
							var old = obj[nodeName];
							obj[nodeName] = [];
							obj[nodeName].push(old);
						}
						obj[nodeName].push(xmlToJSON(item));
					}
				}
			}
			return obj;
		};

		return xmlToJSON(xml);
	}

	/**
	 * Optimize the delay tap when using click eventListener on the mobile device. Remove all the touchmove events on the elements that we don't need it.
	 */
	Utilities.optimizeTouchEvents = function() {
		
		if(Utilities.Browser.isTouch())
		{
			/*
				FastClick is a simple, easy-to-use library for eliminating the 300ms delay between a physical tap and the firing of a click event on mobile browsers. The aim is to make your application feel less laggy and more responsive while avoiding any interference with your current logic.  https://github.com/ftlabs/fastclick 
			*/
			FastClick.attach(document.body);
			
			// prevent touch move events on every single DOM element except the elements which needs to be touched like the overflowed textarea with scrollbar
			jQuery(document).on("touchmove", function(e) {	
				if(jQuery(e.target).css("overflow").toString() != "auto")
				{
					e.preventDefault();
				}
			});
			
				/* turn on when production mode */
				
				// prevent the user from calling the context menu (via right click on desktop)
				jQuery(document).on("contextmenu", function(e) {
					return  false
				});

				jQuery(window).on("contextmenu", function(e) {
					return  false
				});

				jQuery("body").on("contextmenu", function(e) {
					return  false
				});
				
				/* turn on when production mode */
		}

	}

	/**
	 * hide tooltip on a tag when mouse over
	 * @returns {jQuery} all elements which had the possible title and alt attribute when on the hover state 
	 */
	jQuery.fn.hideTips = function(){
	  // when building a plugin, "this" references the jquery object that the plugin was called on.
	  // iterate through each element and return the elements.
	  return this.each(function(){
	        // save element for referencing later
	        var $elem = jQuery(this)
	        // save alt and title
	        var savealt = $elem.attr('alt');
	        var savetitle = $elem.attr('title');
	        // on mouseover, remove attributes
	        // on mouseout, set attributes
	        $elem.hover(function(){
	              $elem.removeAttr('title').removeAttr('alt');
	        },function(){
	              $elem.attr({title:savetitle,alt:savealt});
	        });
	  });
	};


		/**
		* (c) 2012 Steven Levithan <http://slevithan.com/>
		* MIT license
		*/
		if (!String.prototype.codePointAt) {
			String.prototype.codePointAt = function (pos) {
			    pos = isNaN(pos) ? 0 : pos;
			    var str = String(this),
			        code = str.charCodeAt(pos),
			        next = str.charCodeAt(pos + 1);
			    // If a surrogate pair
			    if (0xD800 <= code && code <= 0xDBFF && 0xDC00 <= next && next <= 0xDFFF) {
			        return ((code - 0xD800) * 0x400) + (next - 0xDC00) + 0x10000;
			    }
			    return code;
			};
		}

		/**
		* Encodes special html characters
		* @param string
		* @return {*}
		*/
		Utilities.html_encode = function(string) {
			var ret_val = '';
			for (var i = 0; i < string.length; i++) { 
			    if (string.codePointAt(i) > 127) {
			        ret_val += '&#' + string.codePointAt(i) + ';';
			    } else {
			        ret_val += string.charAt(i);
			    }
			}
			return ret_val;
		}

		/**
		* Get text from XML node, http:// and file:// protocol supported,
		* @param string
		* @return string
		*/
		Utilities.getTextFromXMLNode = function($nodeInstance) { 
            var valueNode = $nodeInstance[0].childNodes[0].nodeValue;
            // windows with Chrome
            valueNode = valueNode.replace("[CDATA[", "");
            valueNode = valueNode.replace("]]", "");
            return valueNode;
		}
	
})(window, jQuery);