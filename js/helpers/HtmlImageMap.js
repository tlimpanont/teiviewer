(function(NA) {

/**
 * HtmlImageMap is an abstraction of HTML <map> Tag. It is created to work with Openlayers coordinate system.
 * @class
 * @classdesc It has the possibility to convert shape coords to Openlayers.Bounds coords.
 * @param {Object} options graphic_url, name, image_width, image_height, graphic_loaded
 */
NA.HtmlImageMap = function(options) {
  var self = this;
  this.graphic_url = options.graphic_url;
  this.name = options.name;
  this.image_width = options.image_width;
  this.image_height = options.image_height;
  this.polygons = new Array();
  this.rectangles = new Array();

  /* if(this.graphic_url != undefined)
  {
      this.image = new Image();
      this.image.src  = this.graphic_url;
      this.image.onload = function() {
          self.image_width = this.width;
          self.image_height = this.height;
          options.graphic_loaded.call(self);
      }
  } */ 
};
/**
 * HtmlImageMap is an abstraction of HTML <map> Tag. It is created to work with Openlayers coordinate system.
 * @returns {String} WCAG 2.0 html of image the image map. Use this as a fallback content for canvas.
 */
NA.HtmlImageMap.prototype.getHtml = function() {
	var id = "graphic_id_" + parseInt(Math.random() * 10000);
	
	var html = "<img src='"+NA.data_folder+this.graphic_url+"' width='"+this.image_width+"' height='"+this.image_height+"' usemap='#"+id+"' alt='"+this.name+"' />";  
	html += "<map name='"+id+"'>";
		for(var i = 0; i < this.rectangles.length; i++)
		{
			html += this.rectangles[i].getAreaHtml();
		}
		
		for(var j = 0; j < this.polygons.length; j++)
		{
			html += this.polygons[j].getAreaHtml();
		}
	html += "</map>";
	return html;
}

/**
 * Represents a rectangle of HtmlImageMap .
 * @constructor This attribute specifies the position and shape on the screen. The number and order of values depends on the shape being defined. Please follow the W3C specs.
 * @param {number} left -  left-x
 * @param {number} top - top-y
 * @param {number} right -  right-x
 * @param {number} bottom - bottom-y
 * @returns {Object}
 */
NA.HtmlImageMap.prototype.rectangle = function(left, top, right, bottom, alternative_text) {
	var self = this;
	var rectangle = {
		/**
		 * @returns {Array}
		*/
		toOpenLayersBoundsCoords: function() {
			return [left, (self.image_height - top), right, (self.image_height - bottom)];
		},
		/**
		 * @returns {String}
		*/
		getAreaHtml: function() {
		  return '<area href="#" shape="rect" coords="' + [left, top, right, bottom].join(",") + '" alt="'+alternative_text+'">';
		}
	}
	this.rectangles.push(rectangle);
	
	return rectangle;
};

/**
 * Represents a polygon of HtmlImageMap.
 * @constructor 
 * @param {String} points Comma seperated polygon points coordinates. eg. 12, 12334, 2033, 223
 * @returns {Object}
 */
NA.HtmlImageMap.prototype.polygon = function(points, alternative_text) {
  var self = this;
  var polygon =  {
    /**
     * @returns {Array}
    */
    toPointsList: function() {
      return points.split(',');
    },
    /**
     * @returns {Array}
    */
    topOpenLayersPointList: function() {
     var _points = points.split(',');

     for(var i = 0; i < _points.length; i++ )
     {
        if(i % 2 == true)
        {
          _points[i] = self.image_height - _points[i];
        }
        else
        {
           _points[i] = parseInt( _points[i]);
        }
     }
     
      return _points;
    },
    /**
     * @returns {String}
    */
    getAreaHtml: function() {
      return '<area href="#" shape="poly" coords="' + points + '" alt="'+alternative_text+'">';
    }
  }
  this.polygons.push(polygon);
  return polygon;
};

})(NA);