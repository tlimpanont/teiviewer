(function(OpenLayers, NA) {

/**
 * There are some coordinates in TEIXml doc we wish to convert. This static class provides handy methods to work with TEI geometry coordinates.
 * @static
 * @class
 */
NA.TEICoordinatesConverter = {};

/**
 * Helpful method to convert list of points to list pair of points.
 * @static
 * @param {String|Array} cords eg. 12,22,33,33 | [12,22,33,33]
 * @returns {Array} comma seperated points in to pairs of points as an Array. eg. 12,22,33,33 ==> [ [12, 22], [33, 33] ]
 */
NA.TEICoordinatesConverter.convertPointsCoordinatesToPairPoints = function(cords) {
        
    if (typeof cords=="string")
    {
        var cords = cords.split(",");


        var cords_array = new Array();
        
        for(var i = 0 ; i < cords.length; i++)
        {
            cords_array.push(parseInt(cords[i]));
        }
    }
    else
    {
        cords_array = cords;
    }


    if((cords_array.length % 2) > 0)
    {
        throw new Error("Not a valid polygon coordinates");
    }


    var array_sets =  parseInt(cords_array.length / 2);

    var pointer = 0;

    var output = new Array();

    for(var j = 0 ; j < array_sets; j++)
    {   
        output[j] = new Array();
        
        
       for(var k = 0; k < 2; k++)
       {
           
            output[j][k] = cords_array[pointer];
            pointer++;
       }
    }

    return output;
}

// FOR FUTURE FUNCTIONALITY, when TEI POINTS ARE DEFINED AS 22,22 22,11 33,11 etc..
NA.TEICoordinatesConverter.convertFromTEIFormat = function(cords) {
  var re = /(\d+\.?)+,(\d+\.?)+/;
  var sourcestring = cords;
  var results = []; var data = new Array();
  var i = 0;
  for (var matches = re.exec(sourcestring); matches != null; matches = re.exec(sourcestring)) {
    results[i] = matches;
    for (var j=0; j<matches.length; j++) {
      data.push(results[i][0]);
    }
    i++;
  }
  return data;
}

})(OpenLayers, NA);