(function() {

	 /**
	 * Provide an interface for navigating through array items. 
	 * @constructor
	 * @class
	 * @classdesc
	 * @param {Array} items Any datatype is acceptable in Array form.
	 * @param {Boolean} infinite If set to true: we can navigate from last to first index like an infinite loop. If it set to false infinite loop is impossible.
	 */ 
	ArrayIterator = function(items, infinite) {
		this.infinite = (infinite == undefined || infinite == "undefined" || infinite == null) ? false : infinite;
		this.items = items;
		this.current_index = -1;
		// start off at index of 0
		this.next();
	};

	/**
	 * Reset current_index. Set the value to -1
	 */ 
	ArrayIterator.prototype.reset = function() {
		this.current_index = -1;
	}

	/**
	 * Go to the next item based on the current item index. If infinite is set to true, inifite loop is posisible.
	 * @returns {*} the item where the current_index is pointing to
	 */ 
	ArrayIterator.prototype.next = function() {
		
		if(this.infinite)
		{
			if(this.getCurrentIndex() >= this.getLastIndex())
			{
				return this.first();
			}
			else
			{
				this.current_index += 1;
				return this.items[this.current_index];
			}
		}
		else
		{	
			var old_index = this.current_index;

			if(!this.hasNext())
			{
				this.current_index = old_index;
				throw new Error("Not allowed to moved to the next item when infinite is set to false. The current index is at the end of the array");
			}
			else
			{
				
				this.current_index += 1;
				return this.items[this.current_index];
			}
		}
	}

	/**
	 * Go to the previous item based on the current item index. If infinite is set to true, inifite loop is posisible.
	 * @returns {*} the item where the current_index is pointing to
	 */ 
	ArrayIterator.prototype.previous = function() {
		if(this.infinite)
		{
			if(this.getCurrentIndex() <= 0)
			{
				return this.last();
			}
			else
			{
				this.current_index -= 1;
				return this.items[this.current_index];
			}
		}
		else
		{	
			var old_index = this.current_index;

			if(!this.hasPrevious())
			{
				this.current_index = old_index;
				throw new Error("Not allowed to moved to the previous item when infinite is set to false. The current index is at the beginning of the array");
			}
			else
			{
				
				this.current_index -= 1;
				return this.items[this.current_index];
			}
		}
	}

	/**
	 * Ask whether it is possible to go to the next item based on the current index
	 * @returns {Boolean}
	 */ 
	ArrayIterator.prototype.hasNext = function() {
		if(this.infinite)
			return true;
		return this.current_index < this.items.length - 1;
	}

	/**
	 * Ask whether it is possible to go to the previous item based on the current index
	 * @returns {Boolean} 
	 */ 
	ArrayIterator.prototype.hasPrevious = function() {
		if(this.infinite)
			return true;
		return this.current_index > 0;
	}

	/**
	 * Set the current index at the first item and return the first item
	 * @returns {*} the item where the current_index is pointing to
	 */ 
	ArrayIterator.prototype.first = function() {
		this.current_index = this.getFirstIndex();
		return this.items[this.current_index];
	}

	/**
	 * Set the current index at the last item and return the last item
	 * @returns {*} the item where the current_index is pointing to
	 */ 
	ArrayIterator.prototype.last = function() {
		this.current_index = this.getLastIndex();
		return this.items[this.current_index];
	}

	/**
	 * Get item by the index number
	 * @params {Number} index
	 * @returns {*} the item where the current_index is pointing to 
	 */ 
	ArrayIterator.prototype.getItemByIndex = function(index) {
		if(this.items[index] == undefined)
			return this.last();
			//throw new Error("Item does not exist in array");
		return this.items[index];
	}
	
	/**
	 * Skip the pointer to the given index
	 * @params {Number} index
	 * @returns {*} the item where the current_index is pointing to
	 */ 
	ArrayIterator.prototype.skipToItemByIndex = function(index) {
		this.current_index = index;
		return this.items[index];
	}

	/**
	 * @returns {Array} All the items 
	 */ 
	ArrayIterator.prototype.getItems = function() {
		return this.items;
	}

	/**
	 * @returns {Number} Get the current index 
	 */ 
	ArrayIterator.prototype.getCurrentIndex = function() {
		return this.current_index;
	}

	/**
	 * @returns {*} the item where the current_index is pointing to
	 */ 
	ArrayIterator.prototype.getCurrentItem = function() {
		return this.items[this.current_index];
	}

	/**
	 * @returns {Number} Get the index number of the last item.  
	 */ 
	ArrayIterator.prototype.getLastIndex = function() {
		return this.items.length - 1;
	}

	/**
	 * @returns {Number} Get the index number of the first item.  
	 */ 
	ArrayIterator.prototype.getFirstIndex = function() {
		return 0;
	}

})();