(function(OpenLayers, NA, jQuery) {

	/**
	 * TEIMap draws NA.TranscriptionHotspotLayer and NA.TranscriptionScanLayer on the map surface. The map settings (resolution, view bounds, max zoomlevel) depend on the scan graphic width and height. 
	 * @constructor 
	 * @augments OpenLayers.Map
	 */
	NA.TEIMap = OpenLayers.Class(OpenLayers.Map, {
		
		/** 
		 * @constructs NA.TEIMap
		 * @param {Object} surface Surface object contains information about graphic and the related zones. 
		 */
		initialize: function(map_id) {
			var self = this;
			var navigation_option = {
				dragPanOptions: {
					enableKinetic: true,
					interval: 0
				},
				documentDrag: true,
				clickOptions: {
					tap: true
				}
			};
			
			/** 
			 * optimize for touch devices when we add TouchNavigation to the map control
			*/

			var navigation_control = (Utilities.Browser.isTouch()) ? new OpenLayers.Control.TouchNavigation(navigation_option) 
																 :  new OpenLayers.Control.Navigation(navigation_option);
			OpenLayers.Map.prototype.initialize.apply(this, [{
				div: map_id,
				controls: [navigation_control]
			}]);
			
			
			this.$map = jQuery("#"+map_id);
			this.$el = this.$map;
			this.$viewport = this.$map.find(".olMapViewport");
			this.$container = this.$viewport.find("div:first");
						
			/** 
			 * remove GPU hardware support. olTileImage class has to be removed from the dom, when we want to desiable transformation calculation. 
			 * Chrome does not display the scanlayer when back-visibility is turned on
			*/
			if(!NA.GPU_SUPPORT)
			{
				this.events.register("move", this, function(e) {
					if(Utilities.BrowserDetect.browser == "Chrome")
					jQuery("img").removeClass("olTileImage");
				});	
				this.events.register("addlayer", this, function(e) {	
					if(Utilities.BrowserDetect.browser == "Chrome")
						jQuery("img").removeClass("olTileImage");
				});
			}
		}
	});


})(OpenLayers, NA, jQuery);


