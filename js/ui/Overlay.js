(function(NA, jQuery, _, Backbone){
	Backbone.View.prototype.eventAggregator = _.extend({}, Backbone.Events);
	NA.Overlay = Backbone.View.extend({
		initialize: function() {	
			this.isOffScreen = false;

			this.setWidthHeight();
			this.setToCenterScreen(true);

			jQuery(window).on("resize", function() {
				this.setWidthHeight();
				this.setToCenterScreen(false);
			}.bind(this));
		},
		events: {
			"click .open_overlay" : function(e) {
				
			},
			"click .close_overlay" : function(e) {
				e.preventDefault();
				this.setToOffScreen();
			}
		},
		setWidthHeight: function() {
			var overlay = jQuery(".overlay");
			var outerMarginX = parseInt(overlay.css("margin-left")) + parseInt(overlay.css("margin-right")) ;
			var outerMarginY = parseInt(overlay.css("margin-top")) + parseInt(overlay.css("margin-bottom"));
			outerMarginX += parseInt(overlay.css("padding-left")) + parseInt(overlay.css("padding-right"));
			outerMarginY += parseInt(overlay.css("padding-top")) + parseInt(overlay.css("padding-bottom"));
			this.$el.width(Utilities.Browser.getSize().width - outerMarginX);
			this.$el.height(Utilities.Browser.getSize().height - outerMarginY);
			
		},
		setToOffScreen: function() {
			var offscreenPositioner = new NA.OffScreenPositioner(this.$el);
			offscreenPositioner.centerTop(function(to) {
				this.animate.call(this, to);
			}.bind(this));
		},
		animate: function(to) {
			jQuery(this.$el).stop(true, false).animate(to, 600, "easeOutQuart", function() {
				this.trigger("overlay:is_off_screen");
				this.isOffScreen = true;
			}.bind(this));
		},
		scaleOut: function() {
			this.$el.transition({ scale: 0 });
			setTimeout(function() {
				this.trigger("overlay:is_off_screen");
			}.bind(this) , 500)
		},
		setToCenterScreen: function(animation) {
			if(animation)
			{
				this.$el.position({
					my: "center center",
					at: "center center",
					of: jQuery(window),
					using: function(animate) {
						jQuery(this).stop(false, true).animate(animate, function() {
							jQuery("a").hideTips();
						});
					}
				});	
			}
			else
			{
				this.$el.position({
					my: "center center",
					at: "center center",
					of: jQuery(window)
				})
			}		
		}
	});
})(NA, jQuery, _, Backbone);