(function(NA, jQuery, _, Backbone){
	Backbone.View.prototype.eventAggregator = _.extend({}, Backbone.Events);


	/**
	 * MapTEIViewer is for the displaying sets of transcription documents, and all the related pages,
	 * @extends  NA.TEIViewer
	 * @class 
	 */
	NA.MapTEIViewer = NA.TEIViewer.extend({
		
		initialize: function() {	
			/* timeouts */
			this.unSelectAllFeaturesTime = NA.unselect_all_features_time;
			this.unSelectAllFeaturesTimeout = null;
			this.themesOverviewTime = NA.themes_overview_time;
			this.themesOverviewTimeout = null;
			this.introductionOverlayTime =  NA.introduction_overlay_time;
			this.introductionOverlayTimeout = null;
			/* end timeouts */

			NA.TEIViewer.prototype.initialize.call(this);


			this.loadAllDocuments( NA.data_folder + "index.xml");

			this.allDocumentsLoaded.done(function(TEIDocuments) {
				this.TEIDocuments = TEIDocuments;

				this.allThemesIds = new Array();

				this.$xml.find("themas thema").each(function(index, thema) {
					this.allThemesIds.push(jQuery(thema).attr("id"));
				}.bind(this));

				this.setUpRouter(
					{
						"": "introduction",
						"themas" : "showThemesOverview",
						":theme_id/:document_id" : "showMap", ":theme_id/:document_id/" : "showMap",
						":theme_id" : "showMapsOfTheme", ":theme_id/" : "showMapsOfTheme",
					}
				);

				this.Router.on("route:introduction", function() {
					this.appendTemplate("#introduction_template");
					this.setBackground(function() {
						this.setUpIntroductionOverlay();
					}.bind(this));
					
				}.bind(this));

				this.Router.on("route:showThemesOverview", function() {
					var self = this;
					this.appendTemplate("#themes_overview_template");

					this.setUpFilmIconForHeader();

					this.setBackground(function(){
						this.setUpGridSliderForThemes();
					}.bind(this));
					
				}.bind(this));

				this.Router.on("route:showMapsOfTheme", function(theme_id) {
					
					var self = this;
					this.appendTemplate("#theme_maps_overview_template");
					

					this.setUpThemeInformationPanel(theme_id);
					this.setUpThemeContextSwitch(theme_id);

					this.setRandomBackground(this.$xml.find("themas thema[id='"+theme_id+"']"), null, function() {
						this.setUpGridSliderForMaps(theme_id);
						
						window.resetTimeouts = function() {
							clearTimeouts();
							createTimeouts();
						}
						
						
						resetTimeouts();

						jQuery("*").on("click mousedown mouseup mousewheel touchstart touchmove", function(e) {	
							resetTimeouts();
						});
				
						jQuery(window).resize(function(e) {
							resetTimeouts();
						});

					
						function clearTimeouts() {
							clearTimeout(self.defaultViewTimeout); 
							self.defaultViewTimeout = null;
						}

						function createTimeouts() {
							if(self.defaultViewTimeout == null)
							{
								self.defaultViewTimeout = setTimeout(function() {
									
									// if the viewer is currently on the document page
									if(jQuery("section#theme_maps_overview").size() > 0  )
									{
										self.Router.navigate("themas");
										window.location.reload();
									}
								}, self.themesOverviewTime);
							}
						}

					}.bind(this), false);

					

				}.bind(this));

				this.Router.on("route:showMap", function(theme_id, document_id) {
					this.theme_id = theme_id;
					this.appendTemplate("#image_map_template");
					this.setUpMapInformationPanel(theme_id, document_id);
					this.setUpMapContextSwitch(theme_id, document_id);
					this.setUpTEIMapLayout();
					this.setUpTEIDocument(theme_id, document_id);
					this.setUpTimeOuts();
				}.bind(this));

				
				Backbone.history.start();


			}.bind(this));

		},
		setUpFilmIconForHeader: function() {
			jQuery("header").find("video").hover(function() {
				jQuery(this).attr("poster", "css/images/btn_introfilm_mini_touched.png"  ); // hover
			}, function() {
				jQuery(this).attr("poster", "css/images/btn_introfilm_mini_default.png"  ); // hover
			});

			new NA.DetailVideoComponent({ el: jQuery("header").find("video"), attributes: {
				content_path: NA.data_folder,
				total_fullscreen: true
			}});
		},
		setUpTimeOuts : function() {
			var self = this;
			jQuery("*").on("click mousedown mouseup mousewheel touchstart touchmove", function(e) {
				resetTimeouts();	
			});

			jQuery(window).resize(function(e) {
				resetTimeouts();
			});

			window.resetTimeouts = function() {
				clearTimeouts();
				createTimeouts();
			}
			
			function clearTimeouts() {
				clearTimeout(self.unSelectAllFeaturesTimeout); 
				self.unSelectAllFeaturesTimeout = null;

				clearTimeout(self.defaultViewTimeout); 
				self.defaultViewTimeout = null;

				clearTimeout(self.documentsOverviewTimeout); 
				self.documentsOverviewTimeout = null;
			}

			function createTimeouts() {
			
				if(self.unSelectAllFeaturesTimeout == null)
				{
					self.unSelectAllFeaturesTimeout = setTimeout(function() {
						
						// if the viewer is currently on the document page
						if(jQuery("section#image_map").size() > 0 && jQuery("#fullscreen_overlay").size() <= 0)
						{
							self.hotspotLayer.unSelectAllFeatures();
							self.detailPanel.setToOffScreen();
						}

					}, self.unSelectAllFeaturesTime);
				}

				if(self.defaultViewTimeout == null)
				{

					self.defaultViewTimeout = setTimeout(function() {
						
						// if the viewer is currently on the document page
						if(jQuery("section#image_map").size() > 0 && jQuery("#fullscreen_overlay").size() <= 0 )
						{
							self.detailPanel.animateOut(function() {
								self.addRemoveLayersAndUnSelectAllFeatures();
							});
						}
					}, self.defaultViewTime);
				}

				if(self.documentsOverviewTimeout == null)
				{
					self.documentsOverviewTimeout = setTimeout(function() {
						
						// if the viewer is currently on the document page
						if(jQuery("section#image_map").size() > 0 && jQuery("#fullscreen_overlay").size() <= 0 )
						{
							self.Router.navigate(self.theme_id);
							window.location.reload();
						}
					}, self.documentsOverviewTime);
				}
			}
		},
		setUpTEIDocument: function(theme_id, document_id) {
			var docReader = NA.TEIViewer.prototype.setUpTEIDocument.apply(this, [document_id, NA.MapDocument, theme_id]);
			this.addRemoveLayersAndUnSelectAllFeatures();
			this.hotspotLayer.POIDetailFolderPath = NA.data_folder + theme_id + "/" + document_id + "/";
			this.detailPanel = new NA.POIDetailPanel({el: jQuery("#poi_detail_panel")});
			this.hotspotLayer.detailPanel = this.detailPanel;
		},
		addRemoveLayersAndUnSelectAllFeatures: function() {
			jQuery(".olAlphaImg").hide();

			NA.TEIViewer.prototype.addRemoveLayersAndUnSelectAllFeatures.call(this , function() {
				
				this.hotspotLayer.addFlagMarkers(jQuery(".olAlphaImg").size() > 0);

			}.bind(this));
		},
		setUpMapInformationPanel: function(theme_id, document_id) {
			this.informationPanel =  new NA.InformationPanel({ el: jQuery("#informationPanel")});
			this.informationPanel.setContent(this.getMapInformationContent(theme_id, document_id));
			jQuery("header h1").html(this.getMapTitle(theme_id, document_id));
		},
		setUpThemeInformationPanel: function(theme_id) {
			this.informationPanel =  new NA.InformationPanel({ el: jQuery("#informationPanel")});
			this.informationPanel.$el.html(this.getThemeInformationContent(theme_id));
			jQuery("header h1").html(this.getThemeTitle(theme_id));	
		},
		setUpIntroductionOverlay: function() {
			var self = this;
			this.introductionOverlay =  new NA.IntroductionOverlay({ el: jQuery("#introduction_overlay")});
			//this.introductionOverlay.setContent(jQuery("#introduction_overlay_template").html());
			//this.introductionOverlay.setContent(this.getIntroductionContent());
			// get the introduction overlay content from the file
			jQuery.get(NA.data_folder + this.$xml.find("intro:first content").attr("src") , function(data) {
				self.introductionOverlay.setContent(data);
			});

			this.introductionOverlay.on("overlay:is_off_screen", function() {
				this.introductionOverlay.$el.parent().remove();
				this.appendTemplate("#themes_overview_template");
				
				jQuery("header").hide().slideToggle(function() {
					this.setBackground(function() {
						this.setUpFilmIconForHeader();
						this.setUpGridSliderForThemes();

					}.bind(this));
				}.bind(this));

			}.bind(this));
		},
		setBackground: function(load_completed) {
			var background_url = this.$xml.find("background").attr("src");
			this.setUpLoader(NA.data_folder+background_url, load_completed);
			//jQuery("body").attr("style", "background: url('"+NA.data_folder+background_url+"');");
			jQuery("body").prepend("<div class='cover_background'></div>");
			
			jQuery(".cover_background").css({
				width : window.innerWidth + "px",
				height: window.innerHeight + "px",
			});
			
			jQuery(".cover_background").css({
				"background-image":  "url('"+NA.data_folder+background_url+"')",
				"background-repeat" : "no-repeat"
			});
		},
		setUpMapContextSwitch: function(theme_id, document_id) {

			var documentsIds = this.getAllDocumentsIdsOfTheme(theme_id);

			var iterator = new ArrayIterator(documentsIds, true);
			iterator.skipToItemByIndex(_.indexOf(documentsIds, document_id));

			jQuery("#next_map").on("click", function(e){
				e.preventDefault();
				iterator.next();
				var next_map = iterator.getCurrentItem();
				
				this.Router.navigate(theme_id + "/" + next_map);
				window.location.reload();
			}.bind(this));

			jQuery("#back_to_maps_overview").attr("href", "#"+theme_id);
			jQuery("#back_to_maps_overview").attr("title", jQuery("#back_to_maps_overview").attr("title") + " van het thema " + this.getThemeTitle(theme_id));
		},
		setUpThemeContextSwitch: function(theme_id) {
			var iterator = new ArrayIterator(this.allThemesIds, true);
			iterator.skipToItemByIndex(_.indexOf(this.allThemesIds, theme_id));


			jQuery("#next_theme").on("click", function(e){
				e.preventDefault();
				iterator.next();
				var next_theme = iterator.getCurrentItem();
				
				this.Router.navigate(next_theme);
				window.location.reload();
			}.bind(this));

		},
		setUpGridSliderForThemes : function() {
			var self = this;
			
			if(sessionStorage.getItem("themes_grid_slider_index") != null)
			{
				var active_index = parseInt(sessionStorage.getItem("themes_grid_slider_index"));
			}
			else {
				var active_index = 0;
			}
				

			var grid_item_options = {
				width: 280,
				height: 315,
				marginX: 20,
				marginY: 40
			};

			var gridSlider = new NA.GridSlider({
				el: jQuery("#grid_slider_container"),
				model: new NA.GridSliderModel({
					rows: 1,
					columns: 3,
					items: getItemModels(this),
				}),
				attributes: {
					item_model: new NA.GridSliderItemModel(grid_item_options),
					control: new NA.GridSliderControl({
						el: jQuery("#slider_control")
					}),
					active_index: active_index
				}
			});

			gridSlider.on("gridslider:slideCompleted", function() {
				sessionStorage.setItem("themes_grid_slider_index", gridSlider.model.get("iterator").getCurrentIndex());
			});

			function getItemModels(context) {
				var items = new Array();
				context.$xml.find("themas thema").each(function(index, thema) {	
					var options = jQuery.extend({
						id: jQuery(thema).attr("id"),
						title:  jQuery(thema).attr("title"),
						thumbnail: jQuery(thema).attr("thumbnail"),
						href: "#"+jQuery(thema).attr("id")
					}, grid_item_options);
					var item_model = new NA.GridSliderItemModel(options);
					items.push(item_model);
				}.bind(context));

				return items;
			}

			window.resetTimeouts = function() {
				clearTimeouts();
				createTimeouts();
			}
			
			resetTimeouts();

			jQuery("*").on("click mousedown mouseup mousewheel touchstart touchmove", function(e) {	
				resetTimeouts();
			});
	
			jQuery(window).resize(function(e) {
				resetTimeouts();
			});

			
			function clearTimeouts() {
				clearTimeout(self.introductionOverlayTimeout); 
				self.introductionOverlayTimeout = null;
			}

			function createTimeouts() {
				if(self.defaultViewTimeout == null)
				{
					self.defaultViewTimeout = setTimeout(function() {
						
						// if the viewer is currently on the document page
						if(jQuery("section#themes_overview").size() > 0 &&  jQuery("#fullscreen_overlay").size() <= 0)
						{
							self.Router.navigate("/");
							window.location.reload();
						}
					}, self.introductionOverlayTime);
				}
			}
		},
		setUpGridSliderForMaps: function(theme_id) {
			
			if(sessionStorage.getItem("maps_grid_slider_index") != null)
			{
				var active_index = parseInt(sessionStorage.getItem("maps_grid_slider_index"));
			}
			else {
				var active_index = 0;
			}

			var grid_item_options = {
				width: 280,
				height: 270,
				marginX: 20,
				marginY: 40
			};

			var gridSlider = new NA.GridSlider({
				el: jQuery("#grid_slider_container"),
				model: new NA.GridSliderModel({
					rows: 2,
					columns: 3,
					items: getItemModels(this),
				}),
				attributes: {
					item_model: new NA.GridSliderItemModel(grid_item_options),
					control: new NA.GridSliderControl({
						el: jQuery("#slider_control")
					}),
					active_index: active_index
				}
			});

			gridSlider.on("gridslider:slideCompleted", function() {
				sessionStorage.setItem("maps_grid_slider_index", gridSlider.model.get("iterator").getCurrentIndex());
			});
			
			function getItemModels(context) {
				var items = new Array();

				context.findTheme(theme_id).find("documents document").each(function(index, document) {
					var options = jQuery.extend({
						id: jQuery(document).attr("id"),
						title:  jQuery(document).attr("title"),
						thumbnail: jQuery(document).attr("thumbnail"),
						href: "#"+theme_id+"/"+jQuery(document).attr("id")
					}, grid_item_options);
					var item_model = new NA.GridSliderItemModel(options);
					items.push(item_model);
				}.bind(context));

				return items;
			}
		},
		getAllDocumentsIdsOfTheme: function(theme_id) {
			var allDocumentsIds = new Array();

			this.findTheme(theme_id).find("documents document").each(function(index, document){
				allDocumentsIds.push(jQuery(document).attr("id"));
			});

			return allDocumentsIds;
		},
		getMapInformationContent: function(theme_id, document_id) {
			var document = this.findDocument(theme_id, document_id);
			var TEIDocument = this.TEIDocuments[document.attr("id")];
			var $node = TEIDocument.$xml.find("facsimile surface note[type='description']");
	 		var text = Utilities.getTextFromXMLNode($node);
			return text;
		},
		getThemeInformationContent: function(theme_id) {
			var $node = this.findTheme(theme_id).find("intro content");
	 		var text = Utilities.getTextFromXMLNode($node);
			return text;
		},
		getIntroductionContent: function() {
			var $node = this.$xml.find("intro:first content");
	 		var text = Utilities.getTextFromXMLNode($node);
			return text;
		},
		getThemeTitle: function(theme_id) {
			return this.findTheme(theme_id).attr("title");
		},
		getMapTitle: function(theme_id, document_id) {
			return this.findDocument(theme_id, document_id).attr("title");
		},
		findDocument : function(theme_id, document_id) {
			return jQuery(this.findTheme(theme_id).find("documents document[id='"+document_id+"']"));
		},
		findTheme: function(theme_id) {
			return jQuery(this.$xml.find("themas thema[id='"+theme_id+"']"));
		}
	});
})(NA, jQuery, _, Backbone);