(function(NA, jQuery, _, Backbone){
	Backbone.View.prototype.eventAggregator = _.extend({}, Backbone.Events);
	NA.IntroductionOverlay = NA.Overlay.extend({
		initialize: function() {	
			NA.Overlay.prototype.initialize.call(this);	
		},
		events: {
			"click .close_overlay" : function(e) {
				e.preventDefault();
				this.setToOffScreen();
			}
		},
		setContent: function(content) {
			this.$el.html(content);

			new NA.DetailVideoComponent({ el: jQuery("video"), attributes: {
				content_path: NA.data_folder,
				total_fullscreen: true
			}});

			this.setUpInteraction();
		},
		setUpInteraction: function() {
			this.$el.find("video").hover(function() {
				jQuery(this).attr("poster", "css/images/btn_introfilm_touched.png"  ); // hover
			}, function() {
				jQuery(this).attr("poster", "css/images/btn_introfilm_default.png"  ); // default
			});

			this.$el.find("div.instruction div.assets a.start").on( "click", function(e) {
				e.preventDefault();
				this.setToOffScreen();
			}.bind(this));
		}
	});
})(NA, jQuery, _, Backbone);