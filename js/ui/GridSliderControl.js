(function(NA, jQuery, _, Backbone){
	Backbone.View.prototype.eventAggregator = _.extend({}, Backbone.Events);
	
	NA.GridSliderControl = Backbone.View.extend({
		initialize: function() {
			this.$el.show();
			this.$el.find("*").show();
		},
		events: {
			"click #next_section" : function(e) {
				e.preventDefault();
				this.gridSlider.nextSection();
			},
			"click #previous_section" : function(e) {
				e.preventDefault();
				this.gridSlider.previousSection();
			}
		},
		setSlider: function(gridSlider) {
			this.gridSlider = gridSlider;
			this.setPosition();
		},
		setPosition: function() {
			
			this.$el.find("#next_section").position({
				my: "right center",
				at: "right+"+jQuery("#next_section").outerWidth()+" center",
				of: this.gridSlider.$el,
				collision: "none"
			});
				
			this.$el.find("#previous_section").position({
				my: "right center",
				at: "left center",
				of: this.gridSlider.$el,
				collision: "none"
			});
		}
	});
})(NA, jQuery, _, Backbone);