(function(NA, jQuery){
	
	NA.OffScreenPositioner = function(elements) {
		this.elements = elements;
	};
	
	NA.OffScreenPositioner.prototype.disableTab = function() {
		this.$el.find("a, button, input[type='buton']").attr("tabindex", -1);
	}
	
	NA.OffScreenPositioner.prototype.animate = function($el, complete_callback) {
		return function animate(to) {
			jQuery($el).stop(false, false).animate(to, complete_callback);
		}
	}
	
	NA.OffScreenPositioner.prototype.left = function(using) {
		jQuery.each(this.elements, function(index, element) {
			jQuery(element).position({
				my: "left top",
				at: "left-"+jQuery(element).outerWidth()+" top",
				of: jQuery(window),
				using: using
			});
		});
		
		return this;
	}
	
	
	NA.OffScreenPositioner.prototype.right = function(using) {
		jQuery.each(this.elements, function(index, element) {
			jQuery(element).position({
				my: "right top",
				at: "right+"+jQuery(element).outerWidth()+" top",
				of: jQuery(window),
				using: using
			});
		});
		
		return this;

	}
	
	NA.OffScreenPositioner.prototype.top = function(using) {
		jQuery.each(this.elements, function(index, element) {
			jQuery(element).position({
				my: "left top",
				at: "left top-"+jQuery(element).outerHeight(),
				of: jQuery(window),
				using: using
			});
		});
		return this;

	}
	
	NA.OffScreenPositioner.prototype.bottom = function(using) {
		jQuery.each(this.elements, function(index, element) {
			jQuery(element).position({
				my: "left bottom",
				at: "left bottom+"+jQuery(element).outerHeight(),
				of: jQuery(window),
				using: using
	
			});
		});
		return this;

	}

	NA.OffScreenPositioner.prototype.arbitary = function(using) {
		jQuery.each(this.elements, function(index, element) {
			jQuery(element).position({
				my: "left bottom",
				at: "left-"+parseInt(Math.random() * 100000) +" bottom-"+parseInt(Math.random() * 100000),
				of: jQuery(window),
				using: using
			});
		});
		return this;

	}
	
	NA.OffScreenPositioner.prototype.centerTop = function(using) {
		jQuery.each(this.elements, function(index, element) {
			jQuery(element).position({
				my: "center bottom",
				at: "center top",
				of: jQuery(window),
				using: using
			});
		});
		return this;

	}
	
	NA.OffScreenPositioner.prototype.centerBottom = function(using) {
		jQuery.each(this.elements, function(index, element) {
			jQuery(element).position({
				my: "center top",
				at: "center bottom",
				of: jQuery(window),
				using: using
			});
		});
	}
	
	NA.OffScreenPositioner.prototype.centerLeft = function(using) {
		jQuery.each(this.elements, function(index, element) {
			jQuery(element).position({
				my: "right center",
				at: "left center",
				of: jQuery(window),
				using: using
			});
		});
		return this;

	}
	
	NA.OffScreenPositioner.prototype.centerRight = function(using) {
		jQuery.each(this.elements, function(index, element) {
			jQuery(element).position({
				my: "left center",
				at: "right center",
				of: jQuery(window),
				using: using
			});
		});
		return this;

	}
	
	NA.OffScreenPositioner.prototype.baseElementRight = function($baseElement, using) {
		jQuery.each(this.elements, function(index, element) {
			console.log($baseElement.position());
			var x_left_space = $baseElement.position().left;
			var x_right_space = Utilities.Browser.getSize().width - ( x_left_space + $baseElement.outerWidth());
			
			jQuery(element).position({
				my: "left top",
				at: "right+"+x_right_space+" top",
				of: $baseElement,
				using: using,
				within: $baseElement
			});
		});
		return this;
	}
	
	NA.OffScreenPositioner.prototype.baseElementLeft = function($baseElement, using) {
		jQuery.each(this.elements, function(index, element) {
			console.log($baseElement.position());

			var x_left_space = $baseElement.position().left;
			var x_right_space = Utilities.Browser.getSize().width - ( x_left_space + $baseElement.outerWidth());
			
			jQuery(element).position({
				my: "right top",
				at: "left-"+x_left_space+" top",
				of: $baseElement,
				using: using,
				within: $baseElement
			});
		});
		return this;
	}


})(NA, jQuery);