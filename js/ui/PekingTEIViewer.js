(function(NA, jQuery, _, Backbone){
	Backbone.View.prototype.eventAggregator = _.extend({}, Backbone.Events);


	/**
	 * PekingTEIViewer is for the displaying sets of transcription documents, and all the related pages,
	 * @extends  NA.TEIViewer
	 * @class 
	 */
	NA.PekingTEIViewer = NA.TEIViewer.extend({
		
		initialize: function() {	
			var self = this;

			NA.TEIViewer.prototype.initialize.call(this);

			this.setUpRouter(
				{
					"": "index",
					":document_id" : "document"
				}
			);

			// when we call a specific document, get id from hash title
			this.Router.on("route:document", function(document_id) {
				var documentReader = new NA.TEIXmlReader();
				
				documentReader.GET({
					url: NA.data_folder + document_id + "/" + document_id + ".xml",
					success: function(request) {
						documentReader.xml_loaded.call(documentReader, request); 
						this.appendTemplate("#peking_map_template");
						// we dont need control GUI when we cant zoom etc..
						var noControls = false;
						this.setUpTEIMapLayout(noControls);
						/**
						 * Show the scan at fullscreen at production screen 1920x1080
						 * @override NA.MapScanLayer.prototype.getMaxResolution
	 					 */	
						NA.MapScanLayer.prototype.getMaxResolution = function() {
							return 4.92;
						}

						// deactivate all the default controls
						for(var i = 0; i < this.map.controls.length; i++)
						{
							var control = this.map.controls[i];
							control.deactivate();
						}

						// drag pan is possible but zoom is not
						var navigation_option = {
							dragPanOptions: {
								enableKinetic: true,
								interval: 0
							},
							documentDrag: true,
							clickOptions: {
								tap: true
							},
							pinchZoomOptions:{
								autoActivate: false 	
							},
							zoomWheelEnabled: false,
							defaultDblClick: function(e) {
								e.preventDefault();
							}
						};

						var navigation_control = (Utilities.Browser.isTouch()) ? new OpenLayers.Control.TouchNavigation(navigation_option)
																 :  new OpenLayers.Control.Navigation(navigation_option);
						// add new controls
						this.map.addControl(navigation_control);
						
						if(Utilities.Browser.isTouch())
							this.map.controls[1].pinchZoom.deactivate();
						
						/**
						 * Set the feature selection style, specific for Peking application
						 * @override NA.MapHotspotLayer.prototype.getStyleMap
	 					 */	
						NA.MapHotspotLayer.prototype.getStyleMap = function() {
							return new OpenLayers.StyleMap({
								"default": new OpenLayers.Style({
								  pointRadius: 10,
								  strokeWidth: 3,
								  strokeOpacity: 1,
								  strokeColor: "#eb660b",
								  fillColor: "#ffcc66",
								  fillOpacity: 0.3
								}),
								"select": new OpenLayers.Style({
								  pointRadius: 10,
								  strokeWidth: 3,
								  strokeOpacity: 1,
								  strokeColor: "#eb660b",
								  fillColor: "black",
								  fillOpacity: 0
								}),
								"temporary": new OpenLayers.Style({
								    fillColor: "#FF99EE",
								    strokeColor: "#CCC",
								    graphicZIndex: 2
								})
					      	});
						}

						
						var TEIDocument = new NA.MapDocument(documentReader, this.map, document_id);
						
						// this.layerGroupsIterator helps to go to next and prev items sequentially, no need to infinite loop, because we get the index from the url
						this.layerGroupsIterator = new ArrayIterator(
								TEIDocument.getLayerGroups());

						// add the correct scan layer with correct hotspots and center to screen
						this.addRemoveLayersAndUnSelectAllFeatures();

						this.hotspotLayer.POIDetailFolderPath = NA.data_folder + "/" + document_id + "/";
						this.detailPanel = new NA.POIDetailPanel({el: jQuery("#poi_detail_panel")});
						//this.detailPanel.$el.find("h3").html(documentReader.$xml.find("teiHeader fileDesc titleStmt title").text());
						this.hotspotLayer.detailPanel = this.detailPanel;

						// when detail panel is gone set it to the default position 
						this.detailPanel.on("panel:is_off_screen", function() {
							this.addRemoveLayersAndUnSelectAllFeatures();
						}.bind(this));
						
						var timeout = null;
						/* end timeouts */

						jQuery("*").on("click mousedown mouseup mousewheel touchstart touchmove", function(e) {
							clearTimeout(timeout); 
							createTimeouts(); 
						});

						jQuery("*").on("mousemove", mouseMove);

						function mouseMove(ev) {
						    if(ev.which==1)
						    {	
						    	clearTimeout(timeout); createTimeouts(); 
						    }					       	
						}

						jQuery(window).resize(function(e) {
							clearTimeout(timeout);
							createTimeouts(); 
						});

						function createTimeouts() {
							timeout = setTimeout(function() {
									self.detailPanel.animateOut();
							} , NA.unselect_all_features_time) 
						}


					}.bind(this)
				});
				
			}.bind(this));
					
			Backbone.history.start();

		}
	});
})(NA, jQuery, _, Backbone);