(function(NA, jQuery, _, Backbone){
	NA.InformationPanel = NA.Panel.extend({
		initialize: function() {			
			NA.Panel.prototype.initialize.call(this);
			
			this.isOffScreen = true;
			this.setWidthHeight();
			

			jQuery(".open_panel").on("click", function(e){
				e.preventDefault();
				this.animateIn();
			}.bind(this));

			jQuery(".close_panel").on("click", function(e){
				e.preventDefault();
				this.animateOut();
			}.bind(this));


			this.on("panel:is_on_screen", function() {
				jQuery("#close_theme_info, #close_map_info").fadeIn(); 
				jQuery("#open_theme_info, #open_map_info").hide();
			});
	
			this.on("panel:is_off_screen", function() {
				jQuery("#close_theme_info, #close_map_info").hide();
				jQuery("#open_theme_info, #open_map_info").fadeIn();
			});

			jQuery(window).on("resize", function(e){
				this.setWidthHeight();
			}.bind(this));
		},
		setContent: function(content) {
			// repect the linebreaks of the editor
			this.$el.html(content.replace(/(\r\n|\n|\r)/gm,"<br />"));
		},
		setWidthHeight: function() {
			this.$el.width(Utilities.Browser.getSize().width - parseInt(jQuery("header li.information").position().left) - (  parseInt(this.$el.css("padding-left")) + parseInt(this.$el.css("padding-right"))  ));
			this.$el.height(window.innerHeight - jQuery("header").outerHeight() - (  parseInt(this.$el.css("padding-top")) + parseInt(this.$el.css("padding-bottom"))  ) );
		},
		setToOffScreen: function(using) {
			this.$el.position({
				my: "left bottom",
				at: "left+"+parseInt(jQuery("header li.information").position().left)+" top",
				of: jQuery(window),
				using: using
			});	
		}
	});
})(NA, jQuery, _, Backbone);