
(function(NA, jQuery, _, Backbone){
	Backbone.View.prototype.eventAggregator = _.extend({}, Backbone.Events);

	/**
	 * TEIViewer is the main visual container that contains multiple UI components
	 * Routing make it possible to navigate via URL through application sections
	 * @extends Backbone.View
	 * @class 
	 */
	NA.TEIViewer = Backbone.View.extend({
		initialize: function() {	
			this.initDataFolder = NA.data_folder; 
			
			this.allDocumentsLoaded = jQuery.Deferred();
			/*### TIMEOUTS */
			this.defaultViewTime = NA.default_view_time;
			this.defaultViewTimeout = null;
			this.documentsOverviewTime = NA.documents_overview_time;
			this.documentsOverviewTimeout = null;

			/*###  END TIMEOUTS */
			this.$documentsXml = null;
			this.Router = null;
			this.transitionBetweenSection = true;
		},
		/**
		 * Routing make it possible to navigate via URL through application sections
		 * @param {Object} Backbone.js Router Object
		 */
		setUpRouter : function(routes_object) {
			var Router = Backbone.Router.extend({
				routes:  routes_object
			});
			this.Router = new Router(); 
		},
		/**
		 * Append html template into the HTML DOM. Transition animation between section is possible
		 * @param {String} template_id The template id found in the HTML dom e.g <script type="text/template" id="documents_overview_template">
		 */
		appendTemplate : function(template_id) {
			jQuery("body").removeAttr("style").html(jQuery(template_id).html());
			jQuery("a").hideTips();
			
			if(this.transitionBetweenSection)
			 	//jQuery("body").hide().fadeIn(1200);

			 this.transitionBetweenSection = true;

		},
		setUpTEIDocument : function(document_id, TEIDocumentClass, folder_name) {
			// query the docReader from the TEIDocuments Array
			
			var folder_name = (folder_name != undefined) ? folder_name + "/" + document_id : document_id;

			var docReader = this.TEIDocuments[document_id];

			// which document are we interested in?
			var TEIDocument = new TEIDocumentClass(docReader, this.map, folder_name);
			// this.layerGroupsIterator helps to go to next and prev items sequentially, no need to infinite loop, because we get the index from the url
			this.layerGroupsIterator = new ArrayIterator(
								TEIDocument.getLayerGroups());

			return TEIDocument;
		},
		setUpTEIMapLayout: function(noControls) {
			setMapHeight(this);
			function setMapHeight(context) {
				jQuery("#"+context.attributes.mapId).height(window.innerHeight);
			}
			jQuery(window).resize(function() {
				setMapHeight(this);
				jQuery(".label_container").css({ width: window.innerWidth});
			}.bind(this));

			this.scanGraphic = null;
			this.scanLayer = null;
			this.hotspotLayer = null;
			this.map = new NA.TEIMap(this.attributes.mapId);

			if(noControls == undefined)
				new NA.ZoomControl({el: jQuery("#zoom_control"), attributes: {
					map: this.map
				}});
			
		},
		loadAllDocuments: function(documents_xml_source) {
			
			var self = this;
			var documents = new Array();
			var counter = 1;
			var documents_count;
		
			jQuery.ajax({
				url: documents_xml_source,
				success: function(xml) {
					this.$xml = jQuery(xml);
					this.$documentsXml = jQuery(xml);
					documents_count = this.$documentsXml.find("documents").find("document").size();
					
					this.$documentsXml.find("documents").find("document").each(function(index, document) {
						var documentReader = new NA.TEIXmlReader();
						documentReader.GET({
							url: NA.data_folder + jQuery(document).attr("teixml"),
							success: function(request) {
								
								documentReader.xml_loaded.call(documentReader, request); 
								documents[jQuery(document).attr("id")] = documentReader;
								if(counter >= documents_count)
									this.allDocumentsLoaded.resolve(documents);
								counter++;
							}.bind(this)
						});
					}.bind(this));
				}.bind(this)	
			});

			this.allDocumentsLoaded.promise(); 
		},
		setRandomBackground: function($rootElement, theme_id, load_completed, tint) {
			$finder = ($rootElement != undefined || $rootElement != null ) ? $rootElement : this.$documentsXml;
			/*  get random document */
			var documents = new Array();
			
			$finder.find("documents document").each(function(index, document) {
				var _document = jQuery(document).attr("id");
				documents.push(_document);
			});
			var document = documents[parseInt(Math.random() * documents.length - 1 )];


			/*  get random page */
			var pages = new Array();
			var docReader = this.TEIDocuments[document];
		
			docReader.$xml.find("facsimile surface graphic").each(function(index, graphic) {
				var _pages = jQuery(graphic).attr("url");
				pages.push(_pages);
			});
			var page = pages[parseInt(Math.random() * pages.length - 1 )];


			if($rootElement != undefined)
			{
				var background_url = NA.data_folder + $rootElement.attr("id") + "/" + document + "/" + page;
			}
			else
			{
				var background_url = NA.data_folder+document+"/"+page;
			}


			this.setUpLoader(background_url, load_completed);
			var tint = (tint) ? "tint" : "";			
			jQuery("body").prepend("<div class='cover_background "+tint+"'></div>");
			
			jQuery(".cover_background").css({
				width : window.innerWidth + "px",
				height: window.innerHeight + "px",
			});
			
			jQuery(".cover_background").css({
				"background-image":  "url('"+background_url+"')",
				"background-repeat" : "no-repeat"
			});
			
		},
		addRemoveLayersAndUnSelectAllFeatures: function(complete_callback) {
			/* null means we reload the page by refresh, if we already have  layers we remove them*/
			if(this.scanLayer != null)
				this.map.removeLayer(this.scanLayer);
			if(this.hotspotLayer != null)
				this.map.removeLayer(this.hotspotLayer); 
			var self = this;	
			/* get the current scan to view the right page of the document */
			this.scan_graphic = this.layerGroupsIterator.getCurrentItem();
			this.scanLayer = this.scan_graphic.scanLayer;
			this.hotspotLayer = this.scan_graphic.hotspotLayer;
			this.map.addLayer(this.scanLayer);
			this.map.addLayer(this.hotspotLayer);
			jQuery("svg").hide();
			
			this.map.setCenter(new OpenLayers.LonLat(0,0));
			this.map.zoomToMaxExtent();
			this.hotspotLayer.unSelectAllFeatures();
			this.scanLayer.addAlternativeTextForImage();

			this.setUpLoader(this.scanLayer.getURL(), function() {
				jQuery("svg").show();
				
				if(complete_callback !== undefined)
					complete_callback();
			});

		},
		setUpLoader: function(source_url, load_completed) {
			var loader = jQuery("<div id='loader_container' class='shadow'>Aan het laden <br /> <br /><center><img class='loader' src='../css/images/loader.gif' /></center></div>");
			
			jQuery("body").append(loader);
		
			jQuery("#loader_container").position({
				at: "center center",
				my: "center center",
				of: jQuery(window)
			});

			jQuery(window).resize(function() {
				jQuery("#loader_container").position({
					at: "center center",
					my: "center center",
					of: jQuery(window)
				});
			});
			
			var scan = new Image();
			scan.onload = function() {
				if(load_completed !== undefined)
					load_completed();	
				loader.remove();
			}

			scan.src = source_url;
		},
		setTEIMapBackground: function() {
			jQuery(this.scanLayer.div).parent().parent().prepend("<div class='cover_background tint'></div>");
			
			jQuery(".cover_background").css({
				width : window.innerWidth + "px",
				height: window.innerHeight + "px",
			});
			
			jQuery(".cover_background").css({
				"background-image":  "url('"+this.scanLayer.getURL()+"')",
				"background-repeat" : "no-repeat"
			});
		},
		animatePageOnToScreen: function(complete_callback) {
			var oldPos = this.map.$container.position();
			/* first move the map off screen */
			this.map.$container.css({
				left: -Utilities.Browser.getSize().width
			});
			
			/* animate the map into the init position defined by OpenLayers */
			if(complete_callback !== undefined)
			{	
				this.map.$container.stop(true, false).animate(oldPos, 900, complete_callback);
			}
			else
			{
				this.map.$container.stop(true, false).animate(oldPos, 900);
			}
		},
		setToDefaultView: function() {
			this.addRemoveLayersAndUnSelectAllFeatures();
			this.detailPanel.setToOffScreen();
			this.animatePageOnToScreen(function() {
				this.detailPanel.animateIn();
			}.bind(this));
			
			document.getSelection().removeAllRanges();
			jQuery(".text_container").scrollTop(0);	
		}
	});

})(NA, jQuery, _, Backbone);