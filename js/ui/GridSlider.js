(function(NA, jQuery, _, Backbone){
	/**
	 * This model is used for deviding sections of the given items. The max items per section is based on the product of columns and columns. 
	 * @constructor
	 * @augments Backbone.Model
	 */
	NA.GridSliderModel = Backbone.Model.extend({
		defaults: {
			items: new Array(),
			sections: new Array(),
			rows: 2,
			columns: 2,
			maxItemsPerSection: null,
			readingDirection: "horizontal",
			position: {
				my: "center center",
				at: "center center",
				of: jQuery(window)
			},
			iterator: null,
			offScreenPositioner: null
		},
		initialize: function() {
			this.set("maxItemsPerSection", this.get("rows") * this.get("columns"));
		},
		/**
		 * Calcualte the sections and devide it into array
		 * @returns {Array} An array of the instances of NA.GridSliderItemModel
		*/
		getSections: function() {
			this.set("sections", new Array() );	

			var sections_count = Math.ceil(this.get("items").length / this.get("maxItemsPerSection"));

			var item_counter = 0;
			
			for(var section_index = 0; section_index < sections_count; section_index++ )
			{
				this.get("sections")[section_index] = new Array();

				for(var i = 0; i < this.get("maxItemsPerSection"); i++ )
				{
					if(this.get("items")[item_counter] !== undefined)
					{
						this.get("sections")[section_index][i] = this.get("items")[item_counter];
					}

					item_counter++;
				}
			}
			return this.get("sections");		
		}
	});
	
	/**
	 * This model represents the item that are in the NA.GridSlider.items
	 * @constructor
	 * @augments Backbone.Model
	 */
	NA.GridSliderItemModel = Backbone.Model.extend({
		defaults: {
			id: null,
			thumbail: "",
			title: "",
			href: ""
		}
	});
	
	/**
	 * This view represents the DOM Object of the NA.GridSlider items. Implement user interaction in this view.
	 * @constructor
	 * @augments Backbone.View
	 */
	NA.GridSliderItem = Backbone.View.extend({
		initialize: function() {
			this.$el.width( this.model.get("width") );
			this.$el.height(this.model.get("height"));
			this.$el.attr("id", this.model.get("id"));
			this.$el.attr("href", this.model.get("href"));
			this.setThumbnail();
			this.setTitle();
		},
		setThumbnail: function() {
			this.$el.find(".thumbnail").attr("style",  "background: url("+NA.data_folder+"/"+this.model.get("thumbnail")+");");
		},
		setTitle: function() {
			this.$el.find("h2").html( this.model.get("title"));
		},
		/**
		 * Position the item in a nice grid based (columns x rows style) fashion
		 */
		position: function(row_index, column_index) {
			var itemLeft = ( this.model.get("width") * column_index ) + ( this.model.get("marginX") * column_index);
			var itemTop  =  ( this.model.get("height") * row_index ) + ( this.model.get("marginY") * row_index);


			var destinationObject = {
				left: itemLeft,
				top: itemTop
			};

			//this.$el.css(destinationObject);

			this.animateIn(destinationObject, itemLeft, itemTop, column_index);
		},
		animateIn : function(destinationObject, itemLeft, itemTop, delay) {
			this.$el.css({
				left: -itemLeft-Utilities.Browser.getSize().width,
				top: 0
			});

			this.$el.delay(0).animate(destinationObject, {
				easing: "easeOutExpo",
				duration: 600
			});
		}
	});
	
	/**
	 * This view contains all the added NA.GridSliderItemModel items
	 * @constructor
	 * @augments Backbone.View
	 */
	NA.GridSlider = Backbone.View.extend({
		initialize: function() {
			
			var viewport = jQuery("<div class='grid_slider_viewport'></div>");
			this.$el.wrap(viewport);

			this.createItems();
			this.setWidthHeight();
			this.setViewport();


			/* iterate through the scans, true is infinite loop */
			this.model.set("iterator", new ArrayIterator(this.model.getSections(), true));

			if(this.attributes.active_index != undefined)
				this.model.get("iterator").skipToItemByIndex(this.attributes.active_index);
		
			this.styleSections();

			/* if a control is attached to the slider we need to let the control know that */
			if(this.attributes.control !== undefined)
				this.attributes.control.setSlider(this);

			jQuery(window).resize(function(e) {
				this.setViewport();

			}.bind(this));

			
			if(this.model.get("sections").length <= 1) {
				jQuery("#slider_control").hide();
			}

		},
		setViewport: function() {
			
			if(jQuery("header").size() > 0)
			{
				jQuery(".grid_slider_viewport").css({
					position: "absolute",
					top: jQuery("header").outerHeight(),
					left: 0,
					width: Utilities.Browser.getSize().width,
					height: Utilities.Browser.getSize().height - jQuery("header").outerHeight()
				});

			}
			else
			{
				jQuery(".grid_slider_viewport").css({
					position: "absolute",
					top: 0,
					left: 0,
					width: Utilities.Browser.getSize().width,
					height: Utilities.Browser.getSize().height 
				});
			}		

			this.$el.position({
				at: "center center",
				my: "center center",
				of: jQuery(".grid_slider_viewport")
			});	
		},
		/**
		 * Set all the sections off screen except the first one, (screen reader accessible)
		 */
		styleSections: function() {
			jQuery(".grid_slider_section").width(this.$el.width()); jQuery(".grid_slider_section").height(this.$el.height());
			
			if(this.attributes.active_index == undefined)
			{
				this.offScreenPositioner = new NA.OffScreenPositioner(
									jQuery(".grid_slider_section").not(":first")).centerLeft();
			}
			else
			{
				this.offScreenPositioner = new NA.OffScreenPositioner(
									jQuery(".grid_slider_section").not(":eq('"+this.attributes.active_index+"')")).centerLeft();	
			}
			
		},
		/**
		 * The container width is based on the total columns and the marginX of the item and the height is based on the total rows and the marginY of the item
		 */
		setWidthHeight: function() {
			this.$el.css({
				width: ( this.attributes.item_model.get("width") ) * this.model.get("columns") + ( this.attributes.item_model.get("marginX") * (this.model.get("columns")- 1)),
				height: ( this.attributes.item_model.get("height") ) * this.model.get("rows") + ( this.attributes.item_model.get("marginY") * (this.model.get("rows") - 1))
			});
		},
		/**
		 * Find the elements we need to work with
		 * @returns {Obejct} The section that need to be pushed off screen and the section that needs to push on screen
		 */
		getIteratorElements : function() {
			var iterator = this.model.get("iterator");
			var old_index = iterator.getCurrentIndex();
			var this_section = iterator.next();
			var next_index = parseInt(iterator.getCurrentIndex());
			var $old_section = jQuery(".grid_slider_section[id='"+old_index+"']");
			var $next_section = jQuery(".grid_slider_section[id='"+next_index+"']");
			
			return {
				$old_section: $old_section,
				$next_section: $next_section
			}
		},
		/**
		 * Go to previous section, move the old one to the left of the screen
		 */
		previousSection: function() {
			var self = this;
			var iteratorElements = this.getIteratorElements();
			//iteratorElements.$next_section.show();
			
			this.offScreenPositioner.elements = iteratorElements.$old_section;
			/* use offScreenPositioner to move the old section to left of the screen*/
			this.offScreenPositioner.baseElementLeft(this.$el, animate_center_to_left);
			
			function animate_center_to_left(to) {
				jQuery(this).stop(true, false).animate(to, function() { 
					self.offScreenPositioner.elements = iteratorElements.$next_section; iteratorElements.$next_section.show();
					/* use offScreenPositioner to move the new section from right on the screen */
					self.offScreenPositioner.baseElementRight(self.$el);
					iteratorElements.$next_section.position({
						my: "left top",
						at: "left top",
						of: self.$el,
						using: animate_right_to_center
					});

					jQuery(this).hide();
				});
			}
			
			function animate_right_to_center(to) {
				jQuery(this).stop(true, false).animate(to, function() {
					self.trigger("gridslider:slideCompleted");	
				});
			}
		},
		/**
		 * Go to next section, move the old one to the right of the screen
		 */
		nextSection: function() {
			var self = this;
			var iteratorElements = this.getIteratorElements();
			//iteratorElements.$next_section.show();
			
			this.offScreenPositioner.elements = iteratorElements.$old_section;
			/* use offScreenPositioner to move the old section to the right of the screen*/
			this.offScreenPositioner.baseElementRight(this.$el, animate_center_to_right);
			
			function animate_center_to_right(to) {
				jQuery(this).stop(true, false).animate(to, function() { 
					self.offScreenPositioner.elements = iteratorElements.$next_section; iteratorElements.$next_section.show();
					/* use offScreenPositioner to move the new section from left on the screen */
					self.offScreenPositioner.baseElementLeft(self.$el);
					iteratorElements.$next_section.position({
						my: "left top",
						at: "left top",
						of: self.$el,
						using: animate_left_to_center
					});

					jQuery(this).hide();
				});
			}
			
			function animate_left_to_center(to) {
				jQuery(this).stop(true, false).animate(to, function() {
					self.trigger("gridslider:slideCompleted");
				});
			}
		},
		/**
		 * Create items and position at the right place in the HTML Dom.
		 */
		createItems: function() {
			
			if(this.model.get("readingDirection") == "horizontal")
			{
				reading_count_upper = this.model.get("rows");
				reading_count_lower = this.model.get("columns");
			}
			if(this.model.get("readingDirection") == "vertical")
			{
				reading_count_upper = this.model.get("columns");
				reading_count_lower = this.model.get("rows");
			}
			
			var sections =  this.model.getSections();


		
			var item_counter = 0;
			
			/* loop through all the sections */
			for(var section_index = 0; section_index < sections.length; section_index++)
			{	
				/* create a HTML element that be a section container for the items */
				var $section = jQuery("<section class='grid_slider_section' id='"+section_index+"'></section>");
				
				/* Inject it into the dom so we can work with the width, height and position properties */
				this.$el.append($section);
				
				/* loop through the items into specific section */
				for(var item_index = 0; item_index < sections[section_index].length; item_index++)
				{
					/* check whether we reach the end of one section */
					if(item_index % this.model.get("maxItemsPerSection") == 0)
					{
						for(var i = 0; i < reading_count_upper; i++)
						{
							
							for(var j = 0; j < reading_count_lower; j++)
							{
								/* only when the item is defined then we want to put it to the DOM */
								if(this.model.get("items")[item_counter] != undefined)
								{
									/* create item based on the template */
									var grid_slider_item =  jQuery('<a href="" class="grid_slider_item"> <div class="thumbnail"></div> <h2></h2> </a>');
									

									/* create an item for each cell of the grid, and give it a readable id on the HTML element */
									var item = new NA.GridSliderItem({ 
										el: grid_slider_item, 
										model: this.model.get("items")[item_counter]
									});
									/* create an item for each cell of the grid */
									item.$el.css(
										{
											/* for development purposes we give each items a random color */
											/* background: '#'+Math.floor(Math.random()*16777215).toString(16), */
											"z-index": 999 - item_counter
										}
									); 	 
									
									/* position the item in a nice grid based on the readingDirection */
									(this.model.get("readingDirection") == "horizontal") ? item.position(i, j) : item.position(j, i);
								
									/* append the item into each section */
									$section.append(item.$el);
									/* couting each items */
									item_counter++;
								}
							} // end reading_count_upper loop ; i
						} // end for reading_count_lower loop ; j
					} 
				} // end each items of each section
			} // end each section
		}
	});
})(NA, jQuery, _, Backbone);