(function(NA, jQuery, _, Backbone){
	Backbone.View.prototype.eventAggregator = _.extend({}, Backbone.Events);
	
	NA.ZoomControl = Backbone.View.extend({
		initialize: function() {
			this.setPosition();
			this.$el.show();
			jQuery(window).on("resize", function(){
				this.setPosition();
			}.bind(this));
		},
		events: {
			"click #zoom_in" : function(e) {
				e.preventDefault();
				this.attributes.map.zoomIn();
			},
			"click #zoom_out" : function(e) {
				e.preventDefault();
				this.attributes.map.zoomOut();
			}
		},
		setPosition: function() {
			
			if(jQuery("header").size() > 0)
			{
				this.$el.position({
					my: "left top",
					at: "left+20 bottom+20",
					of: jQuery("header")
				});
			
			}
			else 
			{
				this.$el.position({
					my: "left top",
					at: "left+20 top+20",
					of: jQuery(window)
				});
			}
		}
	});
})(NA, jQuery, _, Backbone);