(function(NA, jQuery, _, Backbone){
	/**
	 * This panel is used for the transcription teiviewer. The content of this panel is related to the <note type="[]" />
	 * @extends  NA.Panel
	 * @class 
	 */
	NA.RetranslationPanel = NA.Panel.extend({
		initialize: function() {
			NA.Panel.prototype.initialize.call(this);
			
			this.on("panel:is_on_screen", function() {
				
			});
	
			this.on("panel:is_off_screen", function() {
				if(jQuery("header").size() > 0)
				{
					this.$el.find(".open_panel").fadeIn("fast").position({
						my: "right top",
						at: "right-20 bottom+20",
						of: jQuery("header")
					});
				}
				else
				{
					this.$el.find(".open_panel").fadeIn("fast").position({
						my: "right top",
						at: "right-20 top+20",
						of: jQuery(window)
					});
				}
			});

			jQuery(window).on("resize", function() {
					(!this.isOffScreen) ? this.setToInScreen(null) : this.setToOffScreen(null); 
					this.setHeightTextContainer();
			}.bind(this));
		},
		setContent: function(content) {
			this.$el.find(".text_container").html(content);
		}
	});
})(NA, jQuery, _, Backbone);