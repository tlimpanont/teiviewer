(function(NA, jQuery, _, Backbone){
	Backbone.View.prototype.eventAggregator = _.extend({}, Backbone.Events);
	
	NA.Panel = Backbone.View.extend({
		initialize: function() {
			this.isOffScreen = false;
			this.$el.show();
			this.setHeight();
			this.setHeightTextContainer();
			this.setToOffScreen(null);


		
			this.on("panel:is_on_screen", function() {
				this.isOffScreen = false;
				this.$el.find("a, button, input[type='buton']").removeAttr("tabindex");
			});
			
			this.on("panel:is_off_screen", function() {
				this.isOffScreen = true;
				this.$el.find("a, button, input[type='buton']").attr("tabindex", -1);
			});
			
			jQuery(window).on("resize", function() {
					(!this.isOffScreen) ? this.setToInScreen(null) : this.setToOffScreen(null); 
					this.setHeight();
			}.bind(this));
		},
		events: {
			"click .open_panel" : function(e) {
				e.preventDefault();
				jQuery(e.target).hide();
				this.animateIn();
			},
			"click .close_panel" : function(e) {
				e.preventDefault();
				this.animateOut();
			}
		},
		setHeight: function() {
			if(jQuery("header").size() > 0)
			{
				this.$el.css({
					height: parseInt(Utilities.Browser.getSize().height) - parseInt(jQuery("header").outerHeight() + 50) + "px"
				});
			}	
			else
			{
				this.$el.css({
					height: parseInt(Utilities.Browser.getSize().height) + "px"
				});
			}
		},
		setHeightTextContainer: function() {
			if(jQuery("header").size() > 0)
			{
				this.$el.find(".text_container").height(this.$el.outerHeight() - jQuery("header").outerHeight() - parseInt(this.$el.css("padding-bottom")));
			}
			else
			{
				this.$el.find(".text_container").height(Utilities.Browser.getSize().height - 120 );
			}
		},
		animateIn: function(complete_callback) {			
			var self = this;
			this.setToOffScreen(null);
			if(jQuery("#open_retranslation_panel").size() > 0)
				jQuery("#open_retranslation_panel").hide();
			this.setToInScreen(animateIn);
			function animateIn(to) {
				jQuery(self.$el).stop(true, false).animate(to, 500, "easeOutQuad", function() {
					self.trigger("panel:is_on_screen");
					if(complete_callback != undefined) { complete_callback() };
				});
			}
		},
		animateOut: function(complete_callback) {
			var self = this;
			this.setToOffScreen(animateOut);
			function animateOut(to) {
				jQuery(self.$el).stop(true, false).animate(to, 1000, "easeInOutExpo", function() {
					self.trigger("panel:is_off_screen");
					if(complete_callback != undefined) { complete_callback() };
				});
			}
		},
		setToOffScreen: function(using) {
			if(jQuery("header").size() > 0)
			{
				this.$el.position({
					my: "left top",
					at: "right bottom",
					of: jQuery("header"),
					using: using,
					within: jQuery("header")
				});	
			} 
			else 
			{
				this.$el.position({
					my: "left top",
					at: "right top",
					of: jQuery(window),
					using: using,
					within: jQuery(window)
				});	
			}
		},
		setToInScreen: function(using) {
			if(jQuery("header").size() > 0)
			{
				this.$el.position({
					my: "right top",
					at: "right top+"+jQuery("header").outerHeight(),
					of: jQuery(window),
					using: using
				});
			}
			else
			{
				this.$el.position({
					my: "right top",
					at: "right top",
					of: jQuery(window),
					using: using
				});
			}
		}
	});
})(NA, jQuery, _, Backbone);