(function(NA, jQuery, _, Backbone){
	Backbone.View.prototype.eventAggregator = _.extend({}, Backbone.Events);
	
	NA.DetailImageComponent = Backbone.View.extend({
		initialize: function() {
			this.fullscreenDetailImageTimeout = null;
			this.$el.parent().on("click", function(e) {
				e.preventDefault();
				this.showImageOnOverlay(jQuery(e.currentTarget).find("img").not(".expander"));
			}.bind(this));
		},
		showImageOnOverlay: function($source) {
			var self = this;
			// current selected video source
			var image_source = $source.attr("src");
			// make a clone of the video tag
			var full_overlay_image = $source.clone();

			full_overlay_image.attr("id", image_source);
			
			if($source.width() > $source.height() )
			{
				var image_width =  (Utilities.Browser.getSize().width > 1024) ? "75%" : "80%";
			}
			else
			{
				var image_width =  "40%";
			}
			
			full_overlay_image.css({
				width: image_width,
				position: "absolute",
				border: "3px white solid"
			});

			var image = new Image();
			image.src = image_source;
			
			image.onload = function() {
				var fullscreen_overlay = new NA.FullScreenOverlay();
				fullscreen_overlay.addDetailComponent(full_overlay_image);
				if(self.fullscreenDetailImageTimeout != null)
					clearInterval(self.fullscreenDetailImageTimeout); self.fullscreenDetailImageTimeout = null;
				self.fullscreenDetailImageTimeout = setTimeout(function() {
					fullscreen_overlay.removeElements();
				}, parseInt(NA.fullscreen_detail_image_time));
			}
		}
	});

	NA.DetailVideoComponent = Backbone.View.extend({
		initialize: function() {
			this.$el.parent().on("click", function(e) {
				e.preventDefault();
				this.showVideoOnOverlay(jQuery(e.currentTarget).find("video"));
			}.bind(this));
		},
		showVideoOnOverlay: function($source) {
		
			// current selected video source
			var video_source = $source.find("source").attr("src");
			// make a clone of the video tags
			var full_overlay_video =  $source.clone();

		
			full_overlay_video.attr("controls", "true");
			full_overlay_video.attr("autoplay", "true");
			full_overlay_video.attr("id", video_source);
			full_overlay_video.find("source").attr("src", this.attributes.content_path + video_source.replace("#_temp", ""));
				
			if(this.attributes.total_fullscreen)
			{
				full_overlay_video.css({
					width: "100%",
					height: "100%",
					position: "absolute"
				});

				full_overlay_video.removeAttr("controls");
			}
			else
			{
				
				if($source.width() > $source.height() )
				{
					var video_width =  (Utilities.Browser.getSize().width > 1024) ? "75%" : "80%";
				}
				else
				{
					var video_width =  "40%";
				}

				full_overlay_video.css({
					width: video_width,
					position: "absolute"
				});
			}
				

			full_overlay_video.removeAttr("width");
			full_overlay_video.removeAttr("height");
			full_overlay_video.removeAttr("poster");


			full_overlay_video.bind('loadedmetadata',function(){
				var fullscreen_overlay = new NA.FullScreenOverlay();
				fullscreen_overlay.addDetailComponent(full_overlay_video);

				if(this.attributes.total_fullscreen)
				{
					fullscreen_overlay.$detailComponent.on("click", function(e) {
						fullscreen_overlay.removeElements();
					}.bind(this))
				}

				full_overlay_video.bind("ended", function() {
					fullscreen_overlay.removeElements();
				});

			}.bind(this));
		}
	});

	NA.FullScreenOverlay = Backbone.View.extend({
		id: "fullscreen_overlay",
		className: "dark_tint",
		initialize: function() {	
			this.$detailComponent = null;
			this.render();
		},
		events: {
			"click" : function(e) {
				if(e.target === this.$el.get(0))
					this.removeElements();
			},
			"click #close_fullscreen_overlay" : function(e) {
				e.preventDefault();
				this.removeElements();
			} 
		},
		render: function() {
			jQuery(this.el).css({
				width: Utilities.Browser.getSize().width,
				height: Utilities.Browser.getSize().height,
				"z-index": 1002,
				position: "absolute"
			}).prependTo("body").hide().fadeIn();
		},
		addDetailComponent: function(detailComponent) {
			this.$detailComponent = detailComponent;
			this.$el.append('<a href="" title="Het scherm sluiten" id="close_fullscreen_overlay" style="font-size: 3em; color: white;"><span class="icon icon-button-close"></span> </a>');
		
			this.$el.append(detailComponent);
			
			this.repositionElements(detailComponent);
		},
		repositionElements: function(detailComponent) {

			if(detailComponent.has("source").size() > 0) // is html5 video tag
			{
				detailComponent.position({
					at: "center center",
					my: "center center",
					of: jQuery(window)
				});
			}
			else 
			{
				detailComponent.position({
					at: "center center",
					my: "center center",
					of: jQuery(window)
				});
			}


			jQuery("#close_fullscreen_overlay").position({
				my: "right bottom",
				at: "right top",
				of: jQuery(detailComponent)
			});
		},
		removeElements: function() {
			try { window.resetTimeouts(); } catch(e) { }
			this.$detailComponent.remove();
			this.$detailComponent = null;
			this.$el.fadeOut(function() {
				this.remove();
				this.trigger("fullscreen_overlay:removed");
			}.bind(this));
		}
	});
})(NA, jQuery, _, Backbone);