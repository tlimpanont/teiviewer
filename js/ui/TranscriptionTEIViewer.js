(function(NA, jQuery, _, Backbone){
	Backbone.View.prototype.eventAggregator = _.extend({}, Backbone.Events);
	/**
	 * TranscriptionTEIViewer displays sets of transcription documents, and all the related pages.
	 * @extends  NA.TEIViewer
	 * @class 
	 */
	NA.TranscriptionTEIViewer = NA.TEIViewer.extend({
		
		initialize: function() {	
			
			NA.TEIViewer.prototype.initialize.call(this, {});
		
			this.setUpRouter(
				{
					"" : "index",
					":verhaal_id" : "documenten_overzicht",
					":verhaal_id/" : "documenten_overzicht",
					":verhaal_id/:document_id/pagina/:page_number" : "document_pagina",
					":verhaal_id/:document_id/pagina/:page_number/" : "document_pagina"
				}
			);

			this.Router.on("route:index", function() {
				var _this = this;
				this.appendTemplate("#story_selection_template");
				jQuery("#story_selection").find("select").on("change", function(e) {
					window.location.href = document.URL + jQuery(e.target).val();
					window.location.reload();
				});

				function setPositionElement() {
					jQuery("#story_selection").position({
						at: "center center",
						my: "center center",
						of: jQuery(window)
					});

					jQuery("#story_selection").find("select").position({
						at: "center center",
						my: "center center",
						of: jQuery(window)
					});
				}

				setPositionElement();

				jQuery(window).on("resize", function() {
					setPositionElement();
				});


			}.bind(this));

			this.Router.on("route:documenten_overzicht", function(verhaal_id) {
				this.loadDocumentsOfStory(verhaal_id);

				this.allDocumentsLoaded.done(function(TEIDocuments) {
					this.TEIDocuments = TEIDocuments;
					this.appendTemplate("#documents_overview_template");

					
					this.setRandomBackground(null, null, function() {
						this.setUpGridSlider();
					}.bind(this), true);
					
				
				}.bind(this));
				
			}.bind(this));

			this.Router.on("route:document_pagina", function(verhaal_id, document_id, page_number) {
				this.currentPageNumber = page_number;
				
				this.loadDocumentsOfStory(verhaal_id);

				this.allDocumentsLoaded.done(function(TEIDocuments) {
					
					this.TEIDocuments = TEIDocuments;
					this.appendTemplate("#document_template");
					jQuery("#back_to_documents_overview").attr("href", "#"+this.verhaal_id);
						
					this.setUpTEIMapLayout();
					this.detailPanel = new NA.RetranslationPanel({el: jQuery("#retranslation_panel")});
					this.setUpViewDocumentPage(document_id, page_number);
					
				}.bind(this));

			}.bind(this));

			Backbone.history.start();
		},
		loadDocumentsOfStory: function(verhaal_id) {
			this.verhaal_id = verhaal_id;
			
			NA.data_folder = this.initDataFolder;

			NA.data_folder = NA.data_folder + verhaal_id  + "/";

			this.loadAllDocuments(NA.data_folder + verhaal_id+".xml");	

		},
		setUpGridSlider: function() {

			if(sessionStorage.getItem("transcription_grid_slider_index") != null)
			{
				var active_index = parseInt(sessionStorage.getItem("transcription_grid_slider_index"));
			}
			else {
				var active_index = 0;
			}

			// default transcription settings
			if(Utilities.Browser.getSize().width > 1400)
			{
				var grid_item_options = {
					width: 340,
					height: 325,
					marginX: 40,
					marginY: 30
				};

				var rows =  2;
				var columns = 4;
			}
			// staten-generaal settings
			else
			{
				var grid_item_options = {
					width: 280,
					height: 270,
					marginX: 20,
					marginY: 40
				};

				var rows =  2;
				var columns = 4;
			}

			var gridSlider = new NA.GridSlider({
				el: jQuery("#grid_slider_container"),
				model: new NA.GridSliderModel({
					rows: rows,
					columns: columns,
					items: getItemModels(this),
				}),
				attributes: {
					item_model: new NA.GridSliderItemModel(grid_item_options),
					control: new NA.GridSliderControl({
						el: jQuery("#slider_control")
					}),
					active_index: active_index
				}
			});

			gridSlider.on("gridslider:slideCompleted", function() {
				sessionStorage.setItem("transcription_grid_slider_index", gridSlider.model.get("iterator").getCurrentIndex());
			});


			/**
			 * each documents represent a thumb item of the GridSlider
			 * @params {NA.TranscriptionTEIViewer} context Instance of NA.TranscriptionTEIViewer
			 * @returns {Array} An array of NA.GridSliderItemModel instances 
			 */
			function getItemModels(context) {
				var items = new Array();
				context.$documentsXml.find("documents").find("document").each(function(index, document) {
					
					var options = jQuery.extend({
						id: jQuery(document).attr("id"),
						// find the right docReader that can help us to display the correct title
						title:  context.TEIDocuments[jQuery(document).attr("id")].$xml.find("teiHeader fileDesc titleStmt title").text(),
						teixml: jQuery(document).attr("teixml"),
						thumbnail: jQuery(document).attr("thumbnail"),
						href: "#"+context.verhaal_id+"/"+ jQuery(document).attr("id") + "/pagina/1"
					}, grid_item_options);
					var item_model = new NA.GridSliderItemModel(options);
					items.push(item_model);
				}.bind(context));

				return items;
			}
			
		},
		setUpViewDocumentPage: function(document_id, page_number) {
			/* setting up the right document, the docReader can help us */
			this.setUpTEIDocument(document_id);			
			/* page number fetched from url */
			var page_number = parseInt(page_number);
			/* which index of the page collections? */
			var item_index =  (page_number == 0) ? 0 : page_number - 1;
			/* translate index to human readable page number */
			var current_page_number = item_index + 1;
			/* next page number is the number we need when we want to navigate to the next page by click, if at the ends go back to the first page */
			var next_page_number = (current_page_number  >= this.layerGroupsIterator.items.length) ? 1 : current_page_number  + 1;

			/* skip to the item in the array of getTranscriptionLayerGroups (scanLayer and hotspotLayer) */
			this.layerGroupsIterator.skipToItemByIndex(item_index);
			/* able to navigate through pages */
			this.setUpContextSwitch(document_id, current_page_number, next_page_number);
			/* what we do when people want to navigate to the new page */
			this.setUpDefaultViewOfCurrentPage();			
			/* sequence timeouts, get back to default view, and get back to documents overview */
			this.setUpTimeOuts();
		},
		setUpTEIDocument: function(document_id) {
			this.transcriptionDocument = NA.TEIViewer.prototype.setUpTEIDocument.call(this, document_id, NA.TranscriptionDocument);
			
			var docTitle = this.transcriptionDocument.docReader.$xml.find("teiHeader fileDesc titleStmt title").text();
			jQuery("header h1").html(docTitle);
		},
		setUpContextSwitch: function(document_id, current_page_number, next_page_number) {
			/* hide next_page button when theres only one page */
			( this.layerGroupsIterator.items.length == 1) ? jQuery("#next_page").hide() : jQuery("#next_page").show();
			// if we on the last page 
			if(next_page_number == 1)
				jQuery("#next_page").find(".nav_label").html("Eerste pagina");

			jQuery("#total_pages").html(this.layerGroupsIterator.items.length);
			jQuery("#current_page").html(current_page_number);
			
			/*click to go to the next page */
			jQuery("#next_page").on("click", function(e) {
				e.preventDefault();
				this.transitionBetweenSection = false;
				this.Router.navigate(this.verhaal_id+"/"+document_id+"/pagina/"+next_page_number);
				window.location.reload();
				return false;
			}.bind(this));
		},
		setUpDefaultViewOfCurrentPage : function() {
			var self = this;
			
			/* if the read label is existing in the DOM, we want to remove it first before we go to the default view*/
			if(jQuery(".label_container").size() > 0)
			{
				/* first remove the label then change to the default view */
				jQuery(".label_container").fadeOut(function() {
					jQuery(this).remove();
					self.setToDefaultView();
				});
			}
			else
			{
				/* immediately change to the default view */
				this.setToDefaultView();
			}
		},
		setUpTimeOuts : function() {
			var self = this;
			jQuery("*").on("click mousedown mouseup mousewheel touchstart touchmove", function(e) {
			
				/* every click except the click from the next page at the header position */
				if(jQuery(e.target).attr("id") !== "next_page") {
					resetTimeouts();
				}	
			});
			
			jQuery(window).resize(function(e) {
				resetTimeouts();
			});

			function resetTimeouts() {
				clearTimeouts();
				createTimeouts();
			}
			
			function clearTimeouts() {
				clearTimeout(self.defaultViewTimeout); 
				self.defaultViewTimeout = null;

				clearTimeout(self.documentsOverviewTimeout); 
				self.documentsOverviewTimeout = null;
			}

			function createTimeouts() {
				if(self.defaultViewTimeout == null)
				{
					self.defaultViewTimeout = setTimeout(function() {
						
						// if the viewer is currently on the document page
						if(jQuery("section#document").size() > 0 )
						{
							self.addRemoveLayersAndUnSelectAllFeatures();
							if(self.detailPanel.isOffScreen)
								self.detailPanel.animateIn();
						}
					}, self.defaultViewTime);
				}

				if(self.documentsOverviewTimeout == null)
				{
					self.documentsOverviewTimeout = setTimeout(function() {
						
						// if the viewer is currently on the document page
						if(jQuery("section#document").size() > 0 )
						{
							self.Router.navigate(self.verhaal_id);
							window.location.reload();
						}
					}, self.documentsOverviewTime);
				}
			}
		},
		addRemoveLayersAndUnSelectAllFeatures : function() {
			NA.TEIViewer.prototype.addRemoveLayersAndUnSelectAllFeatures.call(this);
			this.setTEIMapBackground();
			this.setPanelContent();
		},
		setPanelContent: function() {
			
			var documentPage = this.transcriptionDocument.getCurrentPage(this.currentPageNumber);
			
			if(	!this.transcriptionDocument.hasDocumentDescription() && 
				!documentPage.hasTranslationText() && 
				!documentPage.hasRetranslationText() &&  
				!documentPage.hasStoryText() )
			{
				this.detailPanel.$el.hide();
				return true;
			}
		
			if(documentPage.hasTranslationText())
			{
				var label = "Vertaling";
				var text = documentPage.getTranslationText();
			}

			if(documentPage.hasRetranslationText())
			{
				var label = "Hertaling";
				var text = documentPage.getRetranslationText();
			}

			if(documentPage.hasStoryText())
			{
				var label = "Verhaal";
				var text = documentPage.getStoryText();
			}
			
			this.detailPanel.$el.find("a#open_retranslation_panel").html(label);
			this.detailPanel.$el.find("h3").html(label);
			this.detailPanel.setContent(text);
			this.detailPanel.$el.show();
			return true;
		}
	});
})(NA, jQuery, _, Backbone);