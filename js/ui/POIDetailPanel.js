(function(NA, jQuery, _, Backbone){
	
	/**
	 * This panel is used for the map teiviewer. The content of this panel can contain a video HTML tag or image HTML tag which can be extended to a larger view.
	 * @extends  NA.RetranslationPanel
	 * @class 
	 */
	NA.POIDetailPanel = NA.RetranslationPanel.extend({
		initialize: function() {

			this.fullscreenOverlay = null;

			NA.Panel.prototype.initialize.call(this);
			
			this.isOffScreen = true;

			this.on("panel:is_on_screen", function() {
				
			});
	
			this.on("panel:is_off_screen", function() {
				
			});
		},
		
		/**
		  * @override NA.RetranslationPanel.setContent
		  */
		setContent: function(content, content_path) {
			
			var contentLoaded = this.filterContent(content, content_path);

			contentLoaded.done(function($newContent) {
				

				NA.RetranslationPanel.prototype.setContent.call(this, $newContent);
				
			 	//Provide a play button image for a video tag
				this.$el.find("video").wrap("<a href='' class='play_button' title='Bekijk deze film'></a>");
				
			  	//Provide a magnify image for a img tag. This lets the user know that the image can be viewed in a biger fashion.
				this.$el.find("img").wrap("<a href='' class='zoom_detail_image' title='Vergroot dit plaatje'></a>");

				this.setPlayButtonForVideo();
				this.setZoomDetailImageButton();

				NA.RetranslationPanel.prototype.setContent.call(this, this.$el.find(".text_container").html());

				// no need to set up video component if we dont have videos
				if(this.$el.find(".text_container").find("video").size() > 0)
				{
					new NA.DetailVideoComponent({el: this.$el.find("video"), attributes: {
						content_path: ""
					}});
				}
				
				// no need to set up image component if we dont have images
				if(this.$el.find(".text_container").find("img").size() > 0)
				{
					new NA.DetailImageComponent({el: this.$el.find("img"), attributes: {
						content_path: ""
					}});
				}

				// remove play button and zoom image if there's no components at all
				if(this.$el.find(".text_container").find("video").size() <= 0)
					jQuery("a.play_button").remove();			
				
				if(this.$el.find(".text_container").find("img").size() <= 0) 
					 jQuery("a.zoom_detail_image").remove();

			}.bind(this));
		},
		/** 
		 * Scan the content of the panel and change everything we needed. The source of the img and video tag are not set tot the right content folder.This method helps to set the content path correctly
		 * @param {String} content The content of the panel
		 * @param {String} content_path Which folder should the panel look for its content?
		 * @returns {jQuery.Deferred().promise()} Promise object to trigger contentLoaded, resolved object is the $newContent
		 */
		filterContent : function(content, content_path) {
			var content = content.replace(/(\r\n|\n|\r)/gm,"");
			var $newContent  = jQuery(jQuery.parseHTML(content));
			
			var contentLoaded = jQuery.Deferred();
			var contentCounter = 0;
			var totalContent = $newContent.filter("video").size() + $newContent.filter("img").size();

			// search for video tags and change the source
			$newContent.filter("video").each(function(index, video) {
				contentCounter++;
				if(contentCounter >= totalContent )
					contentLoaded.resolve($newContent);

				jQuery(video).find("source").attr("src", "#_temp" + content_path + jQuery(video).find("source").attr("src"));
			});

			// search for img tags and change the source
			$newContent.filter("img").each(function(index, image) {
				
				var _image = new Image();
				_image.src = content_path + jQuery(image).attr("src");
				_image.onload = function() {
					contentCounter++;
					if(contentCounter >= totalContent )
						contentLoaded.resolve($newContent);
				}

				jQuery(image).attr("src", content_path + jQuery(image).attr("src"));

			});

			if(totalContent <= 0)
				return contentLoaded.resolve(content);

			return contentLoaded.promise();
		},
		/** 
		 * set the width and height and reposition on the preview video container
		 */
		setPlayButtonForVideo: function() {
			jQuery("a.play_button").each(function(index, play_button) {
				var src = "../css/images/play_alt-64.png";
				$a = jQuery(play_button);
				var $play_button = jQuery("<img alt='Speel het filmpje af icoon' src='"+src+"' style='position: absolute;' class='expander' width='64' height='64' />");
				$a.prepend($play_button);
				$video =  $a.next();
				$a.width($video.width()); $a.height($video.height());

				$play_button.position({
					 my: "center center",
					 at: "center center",
					 of: $a
				});
			});
		},
		/** 
		 * set the width and height and reposition on the preview image container
		 */
		setZoomDetailImageButton: function() {
			jQuery("a.zoom_detail_image").each(function(index, zoom_detail_image) {
				var src = "../css/images/Magnifier2.png";
				var $a = jQuery(zoom_detail_image);
				var $magnify =  jQuery("<img alt='Vergrootglas icoon' src='"+src+"' style='position: absolute;' class='expander' width='64' height='64' />");
				$a.prepend($magnify);
				var $image =  $a.next();
				$a.width($image.width()); $a.height($image.height());
				console.log($image.height());
				$magnify.position({
					 my: "center center",
					 at: "center center",
					 of: $a
				});
	
			});
		}
	});
})(NA, jQuery, _, Backbone);