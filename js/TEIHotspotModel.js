(function(OpenLayers, NA) {

   	NA.TEIRectangleHotspotModel = function() {
   		this.id = null;
		this.naid = null;
		this.name = null;
		this.html_coords = null;
		this.digital_text = null;
		this.area_html = null;
		this.related_graphic_url = null;
		this.openlayers_bounds = new Array();
		this.type = null;	
   	}

	NA.TEIPolygonHotspotModel = function() {
   		this.id = null;
		this.name = null;
		this.html_coords = null;
		this.digital_text = null;
		this.area_html = null;
		this.related_graphic_url = null;
		this.points_list = new Array();
		this.type = null;
		this.naid = null;
   	}
	
})(OpenLayers, NA);