(function(OpenLayers, NA) {
	
	/**
	 * Responsible for creating Hotspots models based on the zones defined in surface. The models can then be used for implementing the Openlayers.Map and Openlayers.Layer 
	 * @constructor
	 * @class
	 * @classdesc
	 * @param {Object} surface The single surface where to focus on.
	 */ 
	NA.TEIHotspotsManager = function(surface) {
		if(Array.isArray(surface))
			throw new Error("Expect to focus on one single surface element in TEIXml document");	

		/** @member {Object} */
		this.surface = surface;
		/** @member {Array} */
		this.zones = jQuery.makeArray(surface.zone);
		/** @member {Object} */
		this.hotspots = new Array();
		/** @member {Array} */
		this.rectangle_hotspots = new Array();
		/** @member {Array} */
		this.polygon_hotspots = new Array();
			
		var document_name = NA.TEIXmlReader.prototype.instance.json.TEI.teiHeader.fileDesc.titleStmt.title['#text'];
		
		this.html_image_map = new NA.HtmlImageMap({
			name: document_name.replace(/^(\s*)|(\s*)$/g, '').replace(/\s+/g, ' '),
			graphic_url:  this.surface.graphic['@attributes'].url,
			image_width:  this.surface['@attributes'].lrx,
			image_height: this.surface['@attributes'].lry
		});

		/** @function Go and analyze surface and create rectangle and polygon hotspots */
		this.createHotspots(this.zones);
	}

	/**
	 * Create new instance of NA.TEIRectangleHotspotModel and push it into NA.TEIHotspotsManager.rectangle_hotspots Array
	 * @param {Object} zone Zone object contain rectangle coordinates: ulx, uly, lrx, lry
	 */ 
	NA.TEIHotspotsManager.prototype.createRectangleHotspot = function(zone) {
		var attributes = zone['@attributes'];
		var id = this.surface['@attributes']["xml:id"];
		var left = parseInt(attributes.ulx), top = parseInt(attributes.uly),  right =  parseInt(attributes.lrx), bottom = parseInt(attributes.lry);

		var hotspot = new NA.TEIRectangleHotspotModel();
		
		var text = (zone.line === undefined) ? this.findTextById(id) : this.findTextByZone(zone);
		//specific feature
		var html_rectangle = this.html_image_map.rectangle(left, top, right, bottom,text);
		hotspot.type = "rect";
		hotspot.openlayers_bounds = html_rectangle.toOpenLayersBoundsCoords();
		hotspot.area_html = html_rectangle.getAreaHtml();
		hotspot.html_coords = attributes.ulx + ", " + attributes.uly + ", "  + attributes.lrx + ", " + attributes.lry;

		//common info
		hotspot.id = id;
		hotspot.naid = zone['@attributes'].naid;
		hotspot.digital_text = text;
		hotspot.related_graphic_url = this.surface.graphic['@attributes'].url;
		hotspot.related_graphic_width = this.html_image_map.image_width;
		hotspot.related_grapic_height = this.html_image_map.image_height;
				
		this.rectangle_hotspots.push(hotspot);
	}

	/**
	 * Create new instance of NA.TEIPolygonHotspotModel and push it into NA.TEIHotspotsManager.polygon_hotspots Array
	 * @param {Object} zone Zone object contain points coordinates which define polygon shape
	 */ 
	NA.TEIHotspotsManager.prototype.createPolygonHotspot = function(zone) {
		var attributes = zone['@attributes'];
		var id = this.surface['@attributes']["xml:id"];
		var left = parseInt(attributes.ulx), top = parseInt(attributes.uly),  right =  parseInt(attributes.lrx), bottom = parseInt(attributes.lry);

		var hotspot = new NA.TEIPolygonHotspotModel();
		var text = (zone.line === undefined) ? this.findTextById(id) : this.findTextByZone(zone);

		//specific feature
		var html_polygon = this.html_image_map.polygon(attributes.points, text);
		hotspot.type = "poly";
		hotspot.points_list = html_polygon.toPointsList();
		hotspot.area_html = html_polygon.getAreaHtml();
		hotspot.html_coords = attributes.points;
		var openlayers_points_coords = html_polygon.topOpenLayersPointList();
		hotspot.points_list = NA.TEICoordinatesConverter.convertPointsCoordinatesToPairPoints(openlayers_points_coords);

		//common info
		hotspot.id = id;
		hotspot.naid = zone['@attributes'].naid;
		hotspot.digital_text = text;
		hotspot.related_graphic_url = this.surface.graphic['@attributes'].url;
		hotspot.related_graphic_width = this.html_image_map.image_width;
		hotspot.related_grapic_height = this.html_image_map.image_height;
		

		this.polygon_hotspots.push(hotspot);

	}


	/**
	 * @param {String} id Text id realated to hotspot in TEIXml doc eg. <line facs="#line1">
	 * @returns {String} The transcribed text related to the hotspot id
 	 */
	NA.TEIHotspotsManager.prototype.findTextById = function(id) {
		var text = NA.TEIXmlReader.prototype.instance.$xml
				.find("text body div lg[facs='#"+this.surface['@attributes']['xml:id']+"']")
				.find("line[facs='#"+id+"']").text();
		return text;
	}
	/**
	 * @param {Object} Zone object from XML
	 * @returns {String} The transcribed text related to the zone
 	 */
	NA.TEIHotspotsManager.prototype.findTextByZone = function(zone) {
		if(zone.line == undefined || zone.line['#text']  == undefined)
		{
			return "";
		}
		else
		{
			var text = zone.line['#text'].replace(/^(\s*)|(\s*)$/g, '').replace(/\s+/g, ' ');
			return text;
		}
	}
	
	/**
	 * Bundle all the created TranscriptionHotspot models in one Array. It contains: NA.TEIRectangleHotspotModel and NA.TEIPolygonHotspotModel models
	 * @param {reader.zones} zones
 	 */	
	NA.TEIHotspotsManager.prototype.createHotspots = function(zones) {	
		for(var i = 0;  i < zones.length; i++ )
		{
			var zone = zones[i];
			var attributes = zone['@attributes'];
			
			if(attributes.lrx != undefined) 
			{
				this.createRectangleHotspot(zone);
			}	

			if(attributes.points != undefined) 
			{
				this.createPolygonHotspot(zone);
			}
			
			// combined rectangle hotspots with the polygon hotspots in one array
			this.hotspots = this.rectangle_hotspots.concat(this.polygon_hotspots);
		}
	}

	/**
	 * @returns {Array} Array of mixed TranscriptionHotspot models (NA.TEIRectangleHotspotModel and NA.TEIPolygonHotspotModel)
 	 */	
	NA.TEIHotspotsManager.prototype.getHotspots = function() {		
		return this.hotspots;
	}

	/**
	 * @returns {Array} Array of NA.TEIPolygonHotspotModel models
 	 */	
	NA.TEIHotspotsManager.prototype.getRectangleHotspots = function() {		
		return this.rectangle_hotspots;
	}

	/**
	 * @returns {Array} Array of NA.TEIRectangleHotspotModel models
 	 */	
	NA.TEIHotspotsManager.prototype.getPolygonHotspots = function() {		
		return this.polygon_hotspots;
	} 

	/**
	 * @returns {String} Concatinated string from the <line /> elements
 	 */	
	NA.TEIHotspotsManager.prototype.getTranscribedText = function() {		
		var transcribed_text  = "";
		for(var i = 0; i < this.hotspots.length; i++)
		{
			var hotspot = this.hotspots[i];
			transcribed_text += hotspot.digital_text.replace(/^(\s*)|(\s*)$/g, '').replace(/\s+/g, ' ');
		}

		return transcribed_text;
	} 

	/**
	 * @returns {String} The retranslated text with linebreaks when possible
 	 */
	NA.TEIHotspotsManager.prototype.getRetranslatedText = function() {		
		if(this.surface.note == undefined || this.surface.note['#text'] == undefined )
		{
			return "";
		}
		else
		{
			if(this.surface.note['@attributes'].type == "retranslation" )
			{
				return  this.surface.note['#text'].replace(/(\r\n|\n|\r)/gm,"<br />");
			}
		}
	}

	/**
	 * @param {String} language The ISO language code, optional
	 * @returns {String} The translated text with linebreaks when possible
 	 */
	NA.TEIHotspotsManager.prototype.getTranslatedText = function(language) {		
		if(this.surface.note == undefined || this.surface.note['#text'] == undefined )
		{
			return "";
		}
		else
		{
			if(this.surface.note['@attributes'].type == "translation" )
			{
				return  this.surface.note['#text'].replace(/(\r\n|\n|\r)/gm,"<br />");
			}
		}
	}  


})(OpenLayers, NA);


