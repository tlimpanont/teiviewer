(function(OpenLayers, NA, jQuery) {
	/**
	 * 
	 * @constructor
	 * @class
	 * @classdesc
	 * @param {NA.TranscriptionScanLayer} 
	 * @param {NA.TranscriptionHotspotLayer} 
	 */ 
	NA.TEIDocumentLayerGroup = function(scanLayer, hotspotLayer) {
		if(!(scanLayer instanceof NA.TranscriptionScanLayer) || !(hotspotLayer instanceof NA.TranscriptionHotspotLayer))
			throw new Error("no instances of NA.TranscriptionScanLayer or NA.TranscriptionHotspotLayer");

		this.scanLayer = scanLayer;
		this.hotspotLayer = hotspotLayer;
	}


	NA.TEIDocument = OpenLayers.Class({
		initialize: function() {
			
		}
	});

	NA.TEIDocument.prototype.createLayerGroups = function(docReader, map, folder, scanLayerClass, hotspotLayerClass) {
		if(Array.isArray(docReader.facsimile))
			throw new Error("Expect to focus on one single facsimile element in TEIXml document");
			this.docReader = docReader;
			this.facsimile = docReader.facsimile;
			this.surfaces =  jQuery.makeArray(docReader.facsimile.surface);
			
			for(var i = 0; i < this.surfaces.length; i++)
			{
			
				var surface = this.surfaces[i];
				var hotspotManager = new NA.TEIHotspotsManager(surface);
				var graphic_name = surface['@attributes']['xml:id'];
				var graphic_width = parseInt(surface['@attributes'].lrx);
				var graphic_height = parseInt(surface['@attributes'].lry);
				var graphic_url = surface.graphic['@attributes'].url;
				
				var scanLayer = new scanLayerClass(graphic_name, graphic_url, graphic_width, graphic_height, folder);
				var hotspotLayer = new hotspotLayerClass(hotspotManager, map);
				this.addLayerGroup(new NA.TEIDocumentLayerGroup(scanLayer, hotspotLayer));
				
			}
	}

	NA.TEIDocument.prototype.getLayerGroups = function() {
		return this.layer_groups;
	}

	NA.TEIDocument.prototype.addLayerGroup = function(layerGroup) {
		this.layer_groups.push(layerGroup);
	}

})(OpenLayers, NA, jQuery);