(function(OpenLayers, NA, jQuery) {

/**
 * @class
 * @classdesc Responsible for loading xml and convert it to JSON. It is a global dataprovider for all the TEIViewer related classes
 */
NA.TEIXmlReader = function() {
	this.xml = null;
	this.json = null;
	this.facsimiles = null;
	this.source = null;
	/**
	 * Call when reader has successfully loaded the xml
	 * @param {OpenLayers.Request.XMLHttpRequest} request
	 */
	this.xml_loaded = function(responseXML) {
		this.xml = responseXML;
		this.$xml = $(this.xml); 
		this.json = Utilities.xmlToJSON(responseXML);
		this.facsimiles = jQuery.makeArray(this.json.TEI.facsimile);
		this.facsimile = this.json.TEI.facsimile;
		NA.TEIXmlReader.prototype.instance = this;
	}
};

/**
 * NA.TEIXmlReader singleton. After the XML is completely loaded, this singleton is available everywhere in the application. 
 * @public
 * @static 	
 */
NA.TEIXmlReader.prototype.instance = null;

/**
 * OpenLayers.Request.GET does not work in IE10, can't get responseXML object, this property is ignored by the unit-test
 * @extends OpenLayers.Request 
 */
NA.TEIXmlReader.prototype.parent = OpenLayers.Request;

/**
 * @borrows NA.TEIXmlReader.prototype.GET as jQuery.ajax
 */
NA.TEIXmlReader.prototype.GET = function(config) {
   /**
 	* fallback use cross browser XMLHTTPRequest implementation of jQuery, HTTPRequest of OpenLayers doesn't work in IE browsers
 	*/
   jQuery.ajax(config);
}

})(OpenLayers, NA, jQuery);


