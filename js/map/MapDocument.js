(function(OpenLayers, NA, jQuery) {
	/**
	 * @extends NA.TEIDocument
	 */ 
	 NA.MapDocument = OpenLayers.Class(NA.TEIDocument, {
	 	/**
	 	 * @override
	 	 */ 
	 	initialize: function(docReader, map, folder) {
	 		NA.TEIDocument.prototype.initialize.call(this);
	 		this.layer_groups = new Array();
	 		this.createLayerGroups(docReader, map, folder, NA.MapScanLayer, NA.MapHotspotLayer);
	 	}
	 });

})(OpenLayers, NA, jQuery);