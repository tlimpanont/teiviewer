(function(OpenLayers, NA) {
	
	/**
	 * 
	 * @constructor 
	 * @augments NA.TranscriptionHotspotLayer
	 */
	NA.MapHotspotLayer = OpenLayers.Class(NA.TranscriptionHotspotLayer, {
		
		/** 
		 * @constructs NA.TranscriptionHotspotLayer
		 * @param {NA.HotspotManager} hotspotManager Instance of NA.HotspotManager.
		 * @param {OpenLayers.Map} [_map] Instance of OpenLayers.Map
		 */
		initialize: function(hotspotManager, _map) {
			NA.TranscriptionHotspotLayer.prototype.initialize.apply(this, [hotspotManager, _map]);
		} 
	});

	/**
	 * @override NA.TranscriptionHotspotLayer.prototype.getStyleMap
	*/
	NA.MapHotspotLayer.prototype.getStyleMap = function() {
		return new OpenLayers.StyleMap({
			"default": new OpenLayers.Style({
			  pointRadius: 10,
			  strokeWidth: 1,
			  strokeOpacity: 1,
			  strokeColor: "red",
			  fillColor: "#ffcc66",
			  fillOpacity: 0.3
			}),
			"select": new OpenLayers.Style({
			  pointRadius: 10,
			  strokeWidth: 3,
			  strokeOpacity: 1,
			  strokeColor: "#01a7e3",
			  fillColor: "black",
			  fillOpacity: 0
			}),
			"temporary": new OpenLayers.Style({
			    fillColor: "#FF99EE",
			    strokeColor: "#CCC",
			    graphicZIndex: 2
			})
      	});
	}

	/**
	 * @override NA.TranscriptionHotspotLayer.prototype.configSelectFeature
	*/
	NA.MapHotspotLayer.prototype.configSelectFeature = function() {
		
		var thisLayer = this;
		var _map = thisLayer._map;

		var oldFeature = null;
		
		var selectCtrl = new OpenLayers.Control.SelectFeature(thisLayer, {
				clickout: false,
				toggle: false,
				onSelect: onSelect,
				onUnselect: onUnselect
			}
		);

		selectCtrl.handlers['feature'].stopDown = false;
    	selectCtrl.handlers['feature'].stopUp = false;

		function onSelect(feature) {
			
			// get the .html page related to the clicked POI
			jQuery.get( thisLayer.POIDetailFolderPath + feature.attributes.naid , function(html) {
				
				thisLayer.detailPanel.setContent(html, thisLayer.POIDetailFolderPath);
					// when content is loaded and set into the detail panel, animate in
				thisLayer.detailPanel.animateIn();


				// what should we do when the detailPanel is off screen?
				thisLayer.detailPanel.on("panel:is_off_screen", function() {
					thisLayer.unSelectAllFeatures();
				});

			});

		}

		function onUnselect(feature) {
			oldFeature = feature;
		}

		thisLayer._map.addControl(selectCtrl);
		selectCtrl.activate();
		this.selectCtrl = selectCtrl;
	}

	/**
	 * Add flag marker on the most top vertice of the feature geometery. Vertices are also OpenLayers.Geometry.Point
	*/
	NA.MapHotspotLayer.prototype.addFlagMarkers = function(flags_visible) {
		jQuery(".olAlphaImg").show();
		var thisLayer = this;
		var markers = new OpenLayers.Layer.Markers( "Markers" );
		this._map.addLayer(markers);

		var size = new OpenLayers.Size(60,50);
		var offset = new OpenLayers.Pixel(0, -size.h);
		var icon = new OpenLayers.Icon('css/images/flag_marker_60_50.png', size, offset);

		// loop through al the geometry points (vertices of the feauters)
		jQuery(this.features).each(function(index, feature) {
			
			// get the polygon vertices and order by its most top coordinates
			var mostTopVerticesY = _.sortBy(feature.geometry.getVertices(), function(vertice) {
				return Math.max(vertice.y);
			}).reverse();

			var myPoint = new OpenLayers.Geometry.Point(mostTopVerticesY[0].x, mostTopVerticesY[0].y );
			// retranslate the vertices in the way that OpenLayers can understand, from OpenLayers.Geometry.Point to new OpenLayers.LonLat
			var myLatLonPoint = myPoint.transform( new OpenLayers.Projection("EPSG:4326"),  this._map.getProjectionObject());
			var marker = new OpenLayers.Marker(new OpenLayers.LonLat(myLatLonPoint.x, myLatLonPoint.y), icon.clone());
			marker.featureId = feature.id;
			
			var _event = (Utilities.Browser.isTouch()) ? "touchstart" : "click";
		  	marker.events.register(_event, marker, function(evt) { 
		  		
		  		thisLayer.unSelectAllFeatures();
				// get the feature id related to the marker where the click events occurs
				selected_feature_id = this.featureId;
				var selected_feature = thisLayer.getFeatureById(selected_feature_id);
				// make this function trigger onSelect event implemented in the NA.MapHotspotLayer.prototype.configSelectFeature method
				thisLayer.selectCtrl.select(selected_feature);
		  		OpenLayers.Event.stop(evt); 
		  	});

			markers.addMarker(marker);
			// attach each marker to each feature with an feature.id, as a html5 data property
			jQuery(marker.events.element).attr("data-feature-id", feature.id);


		}.bind(this));	

		if(!flags_visible) // dont aimate flags when flags are already drawn on the map
			this.animateFlagMarkers();
	}

	/**
	 * Animate flag markers and apply bouncing effects on it.
	*/
	NA.MapHotspotLayer.prototype.animateFlagMarkers = function() {
		// loop through each marker image 
		jQuery(".olAlphaImg").each(function(index, marker) {
			var selected_feature_id = jQuery(marker).parent().attr("data-feature-id");
			// provide an alt tag for web guidline
			jQuery(marker).attr("alt", "Vlag markering bij een Point of Interest");

			// hold its destination position
			var marker_destination_position = jQuery(this).position();
			// but first set it on top off the screen
			
			jQuery(this).css({
				top: -window.innerHeight
			}).show();

			jQuery(this).delay(200 * index).animate(marker_destination_position, 500, "easeOutBounce");
		
		});
	}

})(OpenLayers, NA);


