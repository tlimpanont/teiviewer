/**/(function(OpenLayers, NA) {

	/**
	 * 
	 * @constructor 
	 * @augments NA.TranscriptionScanLayer
	 */
	NA.MapScanLayer = OpenLayers.Class(NA.TranscriptionScanLayer, {
		
		/** 
	     * @override
		 */
		initialize: function(graphic_name, graphic_url, graphic_width, graphic_height, folder) {
			NA.TranscriptionScanLayer.prototype.initialize.apply(this, [graphic_name, graphic_url, graphic_width, graphic_height, folder]);	
		},
		/** 
	     * @override NA.TranscriptionScanLayer.prototype.getNumZoomLevels
		 */
		getNumZoomLevels: function() {
			return 6;
		},
		/**
		 *	@override NA.TranscriptionScanLayer.prototype.getMaxResolution
		 */
		getMaxResolution : function() {
			if(this.isPortrait)
				return ( this.graphic_height / window.innerHeight) + this.getNumZoomLevels();
			return  ( this.graphic_width / window.innerWidth) + this.getNumZoomLevels();
		}
	});
	
	
	
})(OpenLayers, NA);


