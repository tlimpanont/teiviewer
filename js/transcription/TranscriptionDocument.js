(function(OpenLayers, NA, jQuery) {
	/**
	 * 
	 * @constructor
	 * @class
	 * @classdesc
	 * @param {Object} facsimile One single TranscriptionDocument with multiple Graphic elements
	 * @param {OpenLayers.Map} [map] Instance of OpenLayers.Map
	 * @param {String} folder A folder where we can find the graphic. Default is undefined or null.
	 */ 
	 NA.TranscriptionDocument = OpenLayers.Class(NA.TEIDocument, {
	 	/**
	 	 * @override
	 	 */ 
	 	initialize: function(docReader, map, folder) {
	 		NA.TEIDocument.prototype.initialize.call(this);
	 		this.layer_groups = new Array();
	 		this.createLayerGroups(docReader, map, folder, NA.TranscriptionScanLayer, NA.TranscriptionHotspotLayer);
	 	}
	 });

	/**
	 * Get the description of this document 
		<TEI> 
			<teiHeader xml:lang="nl">
			<fileDesc>
				<titleStmt>
				  <title>Bepaling van de Heren XVII</title>
				</titleStmt>
			</fileDesc>
			</teiHeader> 
			<text>
			<p[DOCUMENT DESCIPTION]p>
			</text>
	 * returns {String}
	 */ 
	NA.TranscriptionDocument.prototype.getDocumentDescription = function() {
		var $node = this.docReader.$xml.find("text p");
	 	var text = Utilities.getTextFromXMLNode($node);
		return text.replace(/(\r\n|\n|\r)/gm,"<br />");
	}
	/**
	 * Check whether the docuemnt has a description
	 * returns {Boolean}
	 */ 
	NA.TranscriptionDocument.prototype.hasDocumentDescription = function() {
		return this.docReader.$xml.find("text p").length > 0;
	}
	/**
	 * Get the current page based on the page number. Searhing for the surface "xml:id" attribute value.
	 * returns {NA.TranscriptionDocumentPage} 
	 */ 
	NA.TranscriptionDocument.prototype.getCurrentPage = function(page_number) {
		return new NA.TranscriptionDocumentPage(this.docReader, page_number);
	}
	/**
	 * Represents the current page of the viewed document
	 * @param {Object} facsimile One single TranscriptionDocument with multiple Graphic elements
	 * @param {Number} page_numer current page number from the hash url
	 * @class
	 */
	NA.TranscriptionDocumentPage = function(docReader, page_number) {
		this.docReader = docReader;
		this.$page = this.docReader.$xml.find("surface").filter(function(index, surface) {
				return (jQuery(surface).attr("xml:id") == "page"+page_number)
		});
	}
	/**
	 * Check whether the page has a translation text
	 * @returns {Boolean} 
	 */
	NA.TranscriptionDocumentPage.prototype.hasTranslationText = function() {
		return this.$page.find("note[type='translation']").length > 0;
	}
	/**
	 * Check whether the page has a retranslation text
	 * @returns {Boolean} 
	 */
	NA.TranscriptionDocumentPage.prototype.hasRetranslationText = function() {
		 return this.$page.find("note[type='retranslation']").length > 0;
	}

	/**
	 * Check whether the page has a story text
	 * @returns {Boolean} 
	 */
	NA.TranscriptionDocumentPage.prototype.hasStoryText = function() {
		 return this.$page.find("note[type='story']").length > 0;
	}

	/**
	 * Get the retranslation text of the current document page
	 * @returns {String} 
	 */
	NA.TranscriptionDocumentPage.prototype.getRetranslationText = function() {
		var $node = this.$page.find("note[type='retranslation']");
	 	var text = Utilities.getTextFromXMLNode($node);
		return text.replace(/(\r\n|\n|\r)/gm,"<br />");
	}
			

	/**
	 * Get the translation text of the current document page
	 * @returns {String} 
	 */
	NA.TranscriptionDocumentPage.prototype.getTranslationText = function() {
	 	var $node = this.$page.find("note[type='translation']");
	 	var text = Utilities.getTextFromXMLNode($node);
		return text.replace(/(\r\n|\n|\r)/gm,"<br />");
	}

	/**
	 * Get the story text of the page
	 * @returns {String} 
	 */
	NA.TranscriptionDocumentPage.prototype.getStoryText = function() {
		var $node = this.$page.find("note[type='story']");
	 	var text = Utilities.getTextFromXMLNode($node);
		return text.replace(/(\r\n|\n|\r)/gm,"<br />");
	}

	
})(OpenLayers, NA, jQuery);