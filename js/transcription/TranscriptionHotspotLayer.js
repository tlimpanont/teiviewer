(function(OpenLayers, NA) {
	
	/**
	 * An OpenLayers.Layer.Vector where the vector hotspot features is drawn on. 
	 * @constructor 
	 * @augments OpenLayers.Layer.Vector
	 */
	NA.TranscriptionHotspotLayer = OpenLayers.Class(OpenLayers.Layer.Vector, {
		
		/** 
		 * @constructs NA.TranscriptionHotspotLayer
		 * @param {NA.HotspotManager} hotspotManager Instance of NA.HotspotManager.
		 * @param {OpenLayers.Map} _map Instance of OpenLayers.Map
		 */
		initialize: function(hotspotManager, _map) {
			var self = this;
			this.hotspotManager = hotspotManager;
			this.selectCtrl = null;
			this._map = _map;
		
			
			OpenLayers.Layer.Vector.prototype.initialize.apply(this, [
				null, {
	              styleMap: this.getStyleMap()/*,
	              renderers: ["Canvas"]*/
         	 	}
			]);

			/**
		 	 * When this layer add proper features data, the hotspots will be visible.
		 	 */
		 	var features = this.createFeatures(hotspotManager);
			this.addFeatures(features);
			this.configSelectFeature();
		} 
	});

	NA.TranscriptionHotspotLayer.prototype.getStyleMap = function() {
		return new OpenLayers.StyleMap({
			"default": new OpenLayers.Style({
			  pointRadius: 10,
			  strokeWidth: 0,
			  strokeOpacity: 0.2,
			  strokeColor: "navy",
			  fillColor: "#ffcc66",
			  fillOpacity: 0.2
			}),
			"select": new OpenLayers.Style({
				fillColor: "#614f4d",
				strokeWidth: 0,
				strokeOpacity: 0.2,
				strokeColor: "red",
			    graphicZIndex: 2,
				fillOpacity: 0.2
			}),
			"temporary": new OpenLayers.Style({
			    fillColor: "#FF99EE",
			    strokeColor: "#CCC",
			    graphicZIndex: 2
			})
      	});
	}

	/**
 	 * What happens when we click on a hotspot? This is hotspot onSelect and onUnselect feature implementation
 	 */
	NA.TranscriptionHotspotLayer.prototype.configSelectFeature = function() {
		var thisLayer = this;
		var readLineLabel = null;
		var oldFeature = null;
			
		/** @lends OpenLayers.Control.SelectFeature 
		 * clickout = false, let the user pag-drag even while read label is visible
		*/
		var selectCtrl = new OpenLayers.Control.SelectFeature(thisLayer, {
				clickout: false,
				toggle: true,
				onSelect: onSelect,
				onUnselect: onUnselect
			}
		);

		selectCtrl.handlers['feature'].stopDown = false;
    	selectCtrl.handlers['feature'].stopUp = false;

		function onSelect(feature) {
			readLineLabel = new NA.TranscriptionReadLineLabel(feature, selectCtrl, thisLayer._map);
			readLineLabel.addToHTMLDocument(oldFeature);
		}

		function onUnselect(feature) {
			oldFeature = feature;
			readLineLabel.removeFromHTMLDocument();
			readLineLabel = null;
			
		}

		thisLayer._map.addControl(selectCtrl);
		selectCtrl.activate();
		this.selectCtrl = selectCtrl;
	}

	/**
 	 * 	
 	 */
	NA.TranscriptionHotspotLayer.prototype.unSelectAllFeatures = function() {
		this.selectCtrl.unselectAll();
	}

	/**
 	 * Respect the Web guidline by providing a textual alternative for the canvas and the drawable vector hotspots.
 	 */
	NA.TranscriptionHotspotLayer.prototype.addAlternativeTextForCanvas = function() {
		jQuery(this.div).find("canvas").append(this.hotspotManager.html_image_map.getHtml());
	}
	
	/** 
	 * Create all hotspots features we can find in the dcoument scope of TEIXml.
	 * @param {NA.HotspotManager} hotspotManager Instance of NA.HotspotManager.
	 * @return {Array} A collection of OpenLayers.Feature.Vector objects. 
	 */
	NA.TranscriptionHotspotLayer.prototype.createFeatures = function(hotspotManager) {
		var features = new Array();

		for(var i = 0; i < hotspotManager.hotspots.length; i++) {
			var hotspot = hotspotManager.hotspots[i];

			if(hotspot.type == "rect")
			{
				features.push(this.createFeatureForRectangle(hotspot));
			}

			if(hotspot.type == "poly")
			{
				features.push(this.createFeatureForPolygon(hotspot));
			}
		}

		return features;
	}

	/** 
	 * Create a rectangle hotspot feature.
	 * @param {NA.TranscriptionRectangleHotspot} rectangleHotspot Instance of NA.TranscriptionRectangleHotspot model.
	 * @return {OpenLayers.Feature.Vector} Instance of OpenLayers.Feature.Vector.
	 */
	NA.TranscriptionHotspotLayer.prototype.createFeatureForRectangle = function(rectangleHotspot) {		
		/**
		 * Define bounds area so we can draw hotspot at the right place
		 */
		var rectangle_bounds = new OpenLayers.Bounds( rectangleHotspot.openlayers_bounds);
		
		/**
		 * Define bounds area for vector based label (scale depended object)
		 */
		var label_bounds = new OpenLayers.Bounds(	rectangle_bounds.left, 
													rectangle_bounds.bottom - rectangle_bounds.getHeight(), 
													rectangle_bounds.right, 
													rectangle_bounds.top - rectangle_bounds.getHeight());
		/**
		 * Define bounds area for HTML label. We need this data to convert it to pixel x,y coordinates. (non-scale dependend object)
		 */
		var label_html_bounds = new OpenLayers.Bounds(rectangle_bounds.left - ( rectangle_bounds.getWidth() / 2), 
													rectangle_bounds.bottom - ( rectangle_bounds.getHeight() / 2), 
													rectangle_bounds.right - ( rectangle_bounds.getWidth() / 2), 
													rectangle_bounds.top - ( rectangle_bounds.getHeight() / 2));
		/**
		 * A visible vector hotspot must be a OpenLayers.Feature.Vector
		 */
		var rectangleFeature = new OpenLayers.Feature.Vector(rectangle_bounds.toGeometry());
		
		
		/**
		 * Add additional information to the feature
		 */
		rectangleFeature.attributes.id = rectangleHotspot.id;
		rectangleFeature.attributes.naid = rectangleHotspot.naid;
		rectangleFeature.attributes.digital_text = rectangleHotspot.digital_text;
		rectangleFeature.attributes.label_feature = new OpenLayers.Feature.Vector(label_bounds.toGeometry());
		rectangleFeature.attributes.label_html_bounds = label_html_bounds;
		rectangleFeature.attributes.label_bounds = label_bounds;
	
		
		return rectangleFeature;
	}

	/** 
	 * Create a polygon hotspot feature.
	 * @param {NA.TranscriptionPolygonHotspot} rectangleHotspot Instance of NA.TranscriptionPolygonHotspot model.
	 * @return {OpenLayers.Feature.Vector} Instance of OpenLayers.Feature.Vector.
	 */
	NA.TranscriptionHotspotLayer.prototype.createFeatureForPolygon = function(polygonHotspot) {
		var _points_list = new Array();

		/**
		 * Loop through pair of points and convert it to new OpenLayers.Geometry.Point
		 */
		for(var i = 0; i < polygonHotspot.points_list.length; i++) 
		{
			var point = polygonHotspot.points_list[i];
			_points_list.push( new OpenLayers.Geometry.Point(point[0], point[1])); 
		}

		/**
		 * To draw a polygon based hotspot we need to convert pair of points to OpenLayers.Geometry.LinearRing
		 */
		var linearRing = new OpenLayers.Geometry.LinearRing(_points_list);
		/**
		 * A visible vector hotspot must be a OpenLayers.Feature.Vector. For a polygon we need OpenLayers.Geometry.Polygon 
		 */
		var polygonFeature = new OpenLayers.Feature.Vector( new OpenLayers.Geometry.Polygon([linearRing]));


		/**
		 * Define bounds area for vector based label (scale depended object)
		 */
		var label_bounds = new OpenLayers.Bounds(	polygonFeature.geometry.getBounds().left , 
													polygonFeature.geometry.getBounds().bottom + polygonFeature.geometry.getBounds().getHeight(), 
													polygonFeature.geometry.getBounds().right  , 
													polygonFeature.geometry.getBounds().top + polygonFeature.geometry.getBounds().getHeight());
		/**
		 * Define bounds area for HTML label. We need this data to convert it to pixel x,y coordinates. (non-scale dependend object)
		 */
		var label_html_bounds = new OpenLayers.Bounds(	polygonFeature.geometry.getBounds().left + ( 	polygonFeature.geometry.getBounds().getWidth() / 2), 
														polygonFeature.geometry.getBounds().bottom + ( 	polygonFeature.geometry.getBounds().getHeight() / 2), 
														polygonFeature.geometry.getBounds().right + ( 	polygonFeature.geometry.getBounds().getWidth() / 2), 
														polygonFeature.geometry.getBounds().top + ( 	polygonFeature.geometry.getBounds().getHeight() / 2));
		/**
		 * Add additional information to the feature
		 */
	 	polygonFeature.attributes.id = polygonHotspot.id;
	 	polygonFeature.attributes.naid = polygonHotspot.naid;
		polygonFeature.attributes.digital_text = polygonHotspot.digital_text;											
		polygonFeature.attributes.label_feature = new OpenLayers.Feature.Vector(label_bounds.toGeometry());
		polygonFeature.attributes.label_html_bounds = label_html_bounds;
		polygonFeature.attributes.label_bounds = label_bounds;

		
		return polygonFeature;
	}



})(OpenLayers, NA);


