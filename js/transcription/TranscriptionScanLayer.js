(function(OpenLayers, NA) {

	/**
	 * 
	 * @constructor 
	 * @augments OpenLayers.Layer.Image
	 */
	NA.TranscriptionScanLayer = OpenLayers.Class(OpenLayers.Layer.Image, {
		
		/** 
		 * @constructs NA.TranscriptionScanLayer
		 * @param {String} graphic_name The name of the graphic
		 * @param {String} graphic_url The url of the graphic
		 * @param {Number} graphic_width The width of the graphic
		 * @param {Number} graphic_height The height of the graphic
		 * @param {String} folder A folder where we can find the graphic
		 */
		initialize: function(graphic_name, graphic_url, graphic_width, graphic_height, folder) {
			this.graphic_name = graphic_name;
			this.transcription_text = null;
			this.graphic_width = graphic_width;
			this.graphic_height = graphic_height;
			this.isPortrait = (graphic_height > graphic_width);
			this.folder = (folder !== undefined || folder ===  null) ? folder+"/" : "";
		
			/** Overrides OpenLayers.Layer.Image cunstructor
			 * @param {String} name 
			 * @param {String} url
			 * @param {OpenLayers.Bounds} 
			 * @param {OpenLayers.Size}
			 * @param {Object} options
		 	 */
			OpenLayers.Layer.Image.prototype.initialize.apply(this, [
				graphic_name, 
				NA.data_folder + this.folder + graphic_url, 
				new OpenLayers.Bounds(0,0,this.graphic_width, this.graphic_height), 
				new OpenLayers.Size(this.graphic_width, this.graphic_height), 
				{numZoomLevels: this.getNumZoomLevels(), maxResolution: this.getMaxResolution() , isBaseLayer: true}
			]);
		}

	});
	
	/**
 	 * Respect the Web guidline by providing a textual alternative for the scan image
 	 */
	NA.TranscriptionScanLayer.prototype.addAlternativeTextForImage = function() {
		jQuery(this.div).find("img").attr("alt", this.graphic_name);
	}

	/**
 	 * @returns {Number} The maximum resolution value at which the layer should display
 	 */
	NA.TranscriptionScanLayer.prototype.getMaxResolution = function() {
		// if(this.isPortrait)
		// 	return ( this.graphic_height / window.innerHeight) + this.getNumZoomLevels() / 2 ;
		// return  ( this.graphic_width / window.innerWidth) + this.getNumZoomLevels() / 2 ;

		if(this.isPortrait)
			return this.getNumZoomLevels();
		return  this.getNumZoomLevels();
	}
	/**
 	 * @returns {int} Total number of zoom levels
 	 */
	NA.TranscriptionScanLayer.prototype.getNumZoomLevels = function() {
		if(this.isPortrait)
			return parseInt((this.graphic_height / this.graphic_width) * 4);
		return parseInt((this.graphic_width / this.graphic_height) * 4);
	}
	
	/**
 	  * Calculate the user viewing bounding box. If graphic layout is presented, we based on the layout dimension. 
 	  * Otherwise it should use the window viewport window as the bounding box
 	  * @returns {Object} User view bound is the bounding box where the user can see the document
 	  */
	NA.TranscriptionScanLayer.prototype.getUserViewBound = function() {
		var userViewBound =  {
			x: 0,
			y: 0,
			width: window.innerWidth,
			height: window.innerHeight,
			factor: 4 
		};


		if(jQuery("header").size() > 0 || jQuery(".detail_panel").size() > 0)
		{
			userViewBound = {
				x: jQuery("header").outerHeight()  ,
				y: window.innerHeight,
				width: window.innerWidth - jQuery(".detail_panel").outerWidth(),
				height: window.innerHeight - jQuery("header").outerHeight(),
			};
			
			if(this.isPortrait)
				userViewBound.factor = this.graphic_height / userViewBound.height * this.graphic_width / userViewBound.width;
			userViewBound.factor = this.graphic_width / userViewBound.width * this.graphic_height / userViewBound.height;

			return userViewBound;
		}
		
		return userViewBound;
				
	}
	
})(OpenLayers, NA);


