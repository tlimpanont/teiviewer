(function(OpenLayers, NA, jQuery) {
	
	/**
	 * ReadlLineLabel is a HTML element placed on top/bottom of selected HotspotLayer. Self removing feature when user is not interacting with the scan. 
	 * @constructor 
	 * @param {OpenLayers.Feature.Vector} feature Vector feature of the selected HotspotLayer.
	 * @param {OpenLayers.Control.SelectFeature} selectCtrl Select Feature controller. Implementing select and unselect features.
	 * @param {NA.TranscriptionMap} map  Instance of OpenLayers.Map.
	 */
	NA.TranscriptionReadLineLabel = function(feature, selectCtrl, map) {
		
		this.self_removing_time = NA.self_removing_time_for_read_label;
		this.label_container_class = "label_container";
		this.label_class = "label_text";
		this.timeout = null;
		this.map = map;
		this.$map = map.$map;
		this.label_html_bounds = feature.attributes.label_html_bounds;
		this.digital_text = feature.attributes.digital_text;
		this.id = "label_" + parseInt(Math.random() * 10000);
		this.selectCtrl = selectCtrl;
	}
	
	
	/**
	 * Add the readlabel into the HTML Document. Enabling animation when the old position of the read label is known.
	 * @param {OpenLayers.Feature.Vector} oldFeature The old selected feature. This one will be the origin position object for animation. 
	 */
	NA.TranscriptionReadLineLabel.prototype.addToHTMLDocument = function(oldFeature) {
		var self = this;
		var label_container = jQuery("<div id='"+this.id+"' class='"+this.label_container_class+"'><div class='"+this.label_class+"'>"+this.digital_text+"</div></div>");
		
		
		this.map.$viewport.append(label_container);
		
		this.breakLineForRetranslationPanel();
		
		/**
		 * At which position are we adding the read label to the DOM?
		 */
		var destinationY = parseInt(this.map.getViewPortPxFromLonLat(this.label_html_bounds.getCenterLonLat()).y - jQuery('.label_container').outerHeight());
		
		/**
		 * Animation is not neccessary when oldFeature is unknown
		 */
		if(oldFeature == null)
		{
			label_container.hide();
			
			label_container.css({
				"width": this.$map.width(),
				"top": destinationY + "px"
			});
			/**
		 	 * Let it fade in when we create the new label instance
			 */
			label_container.fadeIn();

			/**
			 * Implement all the map events move, zoomstart, zoomend
			 */
			this.readLineLabelFollowHotspot(label_container);
			/**
			* Start timeout for self removing service
			*/
			this.setTimeOut();
		}
		else
		{
			var oldFeatureY = parseInt(this.map.getViewPortPxFromLonLat(oldFeature.attributes.label_html_bounds.getCenterLonLat()).y - jQuery('.label_container').outerHeight());
			
			/**
			 * Hide label before we animate it
			 */
			label_container.hide();

			/**
			 * Start off this position
			 */
			label_container.css({
				"width": this.$map.width(),
				"top": oldFeatureY + "px"
			});
			/**
			 * Animate to the selected feature position
			 */
			label_container.animate({
				"top": destinationY + "px"
			}, 250, function() {
				/**
				 * Fade in after we completed the animation
				 */
				label_container.fadeIn();
				/**
				 * Implement all the map events move, zoomstart, zoomend
				 */
				self.readLineLabelFollowHotspot(label_container);
				/**
				 * Start timeout for self removing service
				 */
				self.setTimeOut();
			});
		}
	}
	
	NA.TranscriptionReadLineLabel.prototype.breakLineForRetranslationPanel = function() {
		if(jQuery("#retranslation_panel").size() > 0)
		{

			function setWidth() {
				var label_text_width = Utilities.Browser.getSize().width - parseInt(jQuery(".label_text").css("margin-left")) - jQuery("#retranslation_panel").outerWidth();
				jQuery(".label_text").width(label_text_width);
			}

			setWidth();
			
			jQuery(window).resize(function(e) {
				setWidth();
			});
			
		}		
	}
	
	/**
	 * Let the read label follow the hotspot while pan dragged and after zoom is ended.
	 * @param {jQueryHTMLDocument} label_container
	 */
	NA.TranscriptionReadLineLabel.prototype.readLineLabelFollowHotspot = function(label_container) {
		var self = this;
		var viewport_container = this.map.$container;
		/**
	 	 * Calculate the pixel distance of the selected bounding box and the top of the viewport
		 */	            
		var initLabelContainerDistanceY = viewport_container.position().top - label_container.position().top;
		
		this.map.events.register("move", map, function(e) {					
			/**
		 	 * We don't need to see the label when it moves or zoomed in/out
			 */
			label_container.hide();

			/**
		 	 * Postion the label at the correct position
			 */
			label_container.css({
				"top":  parseInt(viewport_container.position().top - initLabelContainerDistanceY) + "px"
			});

			/**
			* Reset the timeout when pan dragged.
			*/
			self.setTimeOut();
		});
		
		this.map.events.register("movestart", map, function(e) { 
			/**
			* We don't want to remove read label when user is zooming the map.
			*/
			clearTimeout(self.timeout);
		});
		
		this.map.events.register("zoomend", map, function(e) {
			/**
		 	 * Postion the label at the correct position
			 */
			repositionReadLabelLine();

			/**
			* Start the timeout when zoom is ended.
			*/
			self.setTimeOut();
			
		});

		this.map.events.register("moveend", map, function(e) {
			/**
		 	 * Postion the label at the correct position
			 */
			repositionReadLabelLine();
				
			/**
			* Start the timeout when zoom is ended.
			*/
			self.setTimeOut();
		});

		/**
		 * This function is used to reposition the label at the correct position
		 */
		function repositionReadLabelLine() {
			var destinationY = parseInt(self.map.getViewPortPxFromLonLat(self.label_html_bounds.getCenterLonLat()).y - jQuery('.label_container').outerHeight());
			label_container.css({
				"top": destinationY + "px"
			});
			label_container.fadeIn();
		}
	}
	/**
	 * Remove the read label from the HTML Document.
	 * @param {Boolean} animation Remove the read label with or without animation (fade effect).
	 */
	NA.TranscriptionReadLineLabel.prototype.removeFromHTMLDocument = function(animation) {
		var animation = (animation == undefined) ? false : animation;
		var self = this;
		/**
		 * This feature is for self removing service. Fade out when user does not interacti with the map.
		 * Declare the timer value via this member instance this.self_removing_time = 30000;
		 */
		if(animation)
		{
			jQuery("#"+this.id).stop().fadeOut(250, function() {
				self.selectCtrl.unselectAll();
				jQuery("#"+self.id).remove();
			});
		}
		else
		{
			this.selectCtrl.unselectAll();
			jQuery("#"+this.id).remove();
		}
		/**
		 * timeout is now useless, so clear it.
		 */
		clearTimeout(this.timeout);
		this.timeout = null;
	}
	
	/**
	 * Build a timeout function. This is for read label self removing service.
	 */
	NA.TranscriptionReadLineLabel.prototype.setTimeOut = function() {
		var self = this;
		if(this.timeout != null)
			clearTimeout(this.timeout);
		
		this.timeout = setTimeout(function() {
			/**
			 * Self removing is with fadeout animation, @param {Boolean} true/false animation
			 */
			self.removeFromHTMLDocument(true);
		}, this.self_removing_time);
	}
	
})(OpenLayers, NA, jQuery);


